package tn.esprit.piDev.socialLearningPortalWEB.managedBeans;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.Course;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Teacher;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.AbstractFacadeLocal;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.CourseFacadeLocal;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.CourseSessionFacadeLocal;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.DocumentFacadeLocal;

@ManagedBean(name = "courseController")
@ViewScoped
public class CourseController extends AbstractController<Course> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	@EJB
	private CourseFacadeLocal facade;
	@EJB
	private CourseSessionFacadeLocal courseSessionFacade;
	@EJB
	private DocumentFacadeLocal documentFacade;
	@ManagedProperty(value = "#{connectionController}")
    private ConnectionController connectionController;

	public CourseController() {
		super(Course.class);
	}
	
	@Override
	public AbstractFacadeLocal<Course> getFacade() {
		return facade;
	}
	@Override
	public void prepareDataForTeacher() {
		setData(facade.findByCreator((Teacher) connectionController.getAccount()));
		recreateModel();
	}
	
	@Override
	public void prepareDataForStudent() {
		// TODO Auto-generated method stub
	}
	@Override
	public void prepareDataForAdmin() {
		// TODO Auto-generated method stub
	}
	
	@Override
	public void prepareCreate(ActionEvent event) {
		Course course = new Course();
		course.setCreator((Teacher) connectionController.getAccount());
		setNewItem(course);
	}
	
	public String showDocuments(){
		return "/pages/document";
	}
	
	public String showSessions(){
		return "/pages/courseSession";
	}

	
	public void setConnectionController(
			ConnectionController connectionController) {
		this.connectionController = connectionController;
	}
	
	@Override
	public ConnectionController getConnectionController() {
		return connectionController;
	}
	
	@Override
	public boolean canEdit() {
		if(getSelectedItem()!=null && getSelectedItem().getCreator() != null && 
				(getSelectedItem().getCreator()).equals((Teacher) connectionController.getAccount())) return true;
		return false;
	}
	@Override
	public boolean canDelete() {
		if(getSelectedItem()!=null && getSelectedItem().getCreator() != null &&
				(getSelectedItem().getCreator()).equals((Teacher)connectionController.getAccount())) return true;
		return false;
	}
	
	

}
