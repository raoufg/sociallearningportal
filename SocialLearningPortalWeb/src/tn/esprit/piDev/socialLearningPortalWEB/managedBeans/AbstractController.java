package tn.esprit.piDev.socialLearningPortalWEB.managedBeans;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.interfaces.IEntity;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.AbstractFacadeLocal;
import tn.esprit.piDev.socialLearningPortalWEB.utils.faces.JsfUtil;
import tn.esprit.piDev.socialLearningPortalWEB.utils.faces.prime.PrimeDataModel;

public abstract class AbstractController<T> {

	private List<T> filteredItems = null;
	private PrimeDataModel<T> items = null;
	private T selectedItem = null;
	private T[] selectedItems = null;
	private T newItem = null;
	private boolean editMode = false;
	private String header, footer;
	private List<T> data;
	private Class<T> entityClass;
	private Map<String, String> requestParametersMap;

	public AbstractController(Class<T> entityClass) {
		this.entityClass = entityClass;
	}
	
	@PostConstruct
	public void init(){
		requestParametersMap = FacesContext.getCurrentInstance()
                .getExternalContext().getRequestParameterMap();
		if(getConnectionController().getAccount()!= null){
			if(getConnectionController().isAdmin()) prepareDataForAdmin();
			if(getConnectionController().isStudent()) prepareDataForStudent();
			if(getConnectionController().isTeacher()) prepareDataForTeacher();
		}
	}
	
	public String getRequestParameter(String parameter){
		return requestParametersMap.get(parameter);
	}
	
	public abstract AbstractFacadeLocal<T> getFacade();
	
	public abstract void prepareDataForStudent();
	
	public abstract void prepareDataForTeacher();
	
	public abstract void prepareDataForAdmin();
	
	public abstract boolean canEdit();
	public abstract boolean canDelete();
	
	public List<T> getData(){
		return getFacade().findAll();
	}
	
	public void setData(List<T> data){
		this.data = data;
	}
	
	public PrimeDataModel<T> getItems() {
		if (items == null) {
			
			if(data == null) return null;
			
			items = new PrimeDataModel<T>(data) {

				@Override
				public Object getRowKey(T object) {
					return ((IEntity)object).getId();
				}

				@Override
				public T getRowData(String rowKey) {
					return getFacade().find(Integer.parseInt(rowKey));
				}

			};
		}
		return items;
	}
	
	public void recreateModel() {
		items = null;
	}
	

	public abstract void prepareCreate(ActionEvent event);
	
	public void create(ActionEvent event){
		getFacade().create(newItem);
		if(data!=null) data.add(newItem);
		else {
			data = new ArrayList<T>();
			data.add(newItem);
		}
		recreateModel();
	}
	
	public void prepareEdit(ActionEvent event){
		editMode = true;
	}
	
	public void edit(ActionEvent event){
		getFacade().edit(selectedItem);
		editMode = false;
	}
	
	public void delete(ActionEvent event){
		
		try {
			getFacade().remove(selectedItem);
			selectedItem = null;
			data.remove(selectedItem);
			editMode = false;
			recreateModel();
		} catch (Exception e) {
			JsfUtil.addErrorMessage(e, "Delete Failed!");
		}
	}
	
	
	public List<T> getFilteredItems() {
		return filteredItems;
	}

	public void setFilteredItems(List<T> filteredItems) {
		this.filteredItems = filteredItems;
	}

	public void setItems(PrimeDataModel<T> items) {
		this.items = items;
	}

	public T getSelectedItem() {
		return selectedItem;
	}

	public void setSelectedItem(T selectedItem) {
		this.selectedItem = selectedItem;
	}

	public T[] getSelectedItems() {
		return selectedItems;
	}

	public void setSelectedItems(T[] selectedItems) {
		this.selectedItems = selectedItems;
	}

	public T getNewItem() {
		if(newItem == null)
			try {
				newItem = entityClass.newInstance();
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		return newItem;
	}

	public void setNewItem(T newItem) {
		this.newItem = newItem;
	}

	public boolean isEditMode() {
		return editMode;
	}

	public void setEditMode(boolean editMode) {
		this.editMode = editMode;
	}

	public void setFooter(String footer) {
		this.footer = footer;
	}
	
	public String getFooter() {
		return footer;
	}
	
	public String getHeader() {
		return header;
	}
	public void setHeader(String header) {
		this.header = header;
	}
	
	public abstract ConnectionController getConnectionController();
	
}
