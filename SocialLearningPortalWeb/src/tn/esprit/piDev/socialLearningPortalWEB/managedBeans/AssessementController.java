package tn.esprit.piDev.socialLearningPortalWEB.managedBeans;

import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.Assessement;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.CourseSession;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Teacher;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.AbstractFacadeLocal;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.AssessementFacadeLocal;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.CourseSessionFacadeLocal;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.DocumentFacadeLocal;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.StudentFacadeLocal;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.TeacherFacadeLocal;

@ManagedBean(name = "assessementController")
@ViewScoped
public class AssessementController extends AbstractController<Assessement> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	@EJB
	private AssessementFacadeLocal facade;
	
	@ManagedProperty(value = "#{connectionController}")
    private ConnectionController connectionController;
	
    private CourseSession selectedCourseSession;
	
	@EJB
	private DocumentFacadeLocal documentFacade;
	@EJB 
	private TeacherFacadeLocal teacherFacade;
	@EJB
	private StudentFacadeLocal studentFacade;
	@EJB
	private AssessementFacadeLocal assessementFacade;
	@EJB
	private CourseSessionFacadeLocal courseSessionFacade;

	public AssessementController() {
		super(Assessement.class);
	}
	
	@Override
	public AbstractFacadeLocal<Assessement> getFacade() {
		return facade;
	}
	
	public void setConnectionController(
			ConnectionController connectionController) {
		this.connectionController = connectionController;
	}
	
	public ConnectionController getConnectionController() {
		return connectionController;
	}
	
	@Override
	public void prepareCreate(ActionEvent event) {
		Assessement assessement = new Assessement();
		assessement.setCourseSession(selectedCourseSession);
		setNewItem(assessement);
	}
	@Override
	public void prepareDataForStudent() {
		// TODO Auto-generated method stub
	}
	@Override
	public void prepareDataForTeacher() {
		Assessement newItem = new Assessement();
		setNewItem(newItem);
		if(getRequestParameter("courseSessionId")!=null){
			int courseSessionId = Integer.parseInt(getRequestParameter("courseSessionId"));
			CourseSession courseSession = courseSessionFacade.find(courseSessionId);
			setData(facade.findByCourseSession(courseSession));
			
			newItem.setCourseSession(courseSession);
			recreateModel();
		}else{
			setData(facade.findByTeacher((Teacher) connectionController.getAccount()));
			recreateModel();
		}
	}
	@Override
	public void prepareDataForAdmin() {
		// TODO Auto-generated method stub
	}
	@Override
	public boolean canEdit() {
		if(getSelectedItem()!=null && getSelectedItem().getCourseSession() != null&& getSelectedItem().getCourseSession().getCreator() != null &&
				getSelectedItem().getCourseSession().getCreator().equals(connectionController.getAccount())) return true;
		return false;
	}
	@Override
	public boolean canDelete() {
		if(getSelectedItem()!=null && getSelectedItem().getCourseSession() != null&& getSelectedItem().getCourseSession().getCreator() != null &&
				getSelectedItem().getCourseSession().getCreator().equals(connectionController.getAccount())) return true;
		return false;
	}
	public CourseSession getSelectedCourseSession() {
		return selectedCourseSession;
	}
	public void setSelectedCourseSession(CourseSession selectedCourseSession) {
		this.selectedCourseSession = selectedCourseSession;
	}
	
	public String showStudents(){
		return "/pages/student";
	}
	

}
