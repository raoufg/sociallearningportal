package tn.esprit.piDev.socialLearningPortalWEB.managedBeans;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.jboss.security.Base64Encoder;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.FlowEvent;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.Account;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.File;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Student;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Teacher;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.AbstractFacadeLocal;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.FileFacadeLocal;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.StudentFacadeLocal;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.TeacherFacadeLocal;
import tn.esprit.piDev.socialLearningPortalWEB.utils.MD5;
import tn.esprit.piDev.socialLearningPortalWEB.utils.faces.JsfUtil;

@ManagedBean(name = "accountController")
@ViewScoped
public class AccountController extends AbstractController<Account> implements
		Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */

	// private static Logger logger =
	// Logger.getLogger(AccountController.class.getName());
	private Account account;
	private Student student;
	private String confirmPassword;
	private String passwd;
	private String oldPasswd;
	private int sexValue = 0;
	@EJB
	private StudentFacadeLocal studentFacade;
	@EJB
	private TeacherFacadeLocal teacherFacade;
	@EJB
	private FileFacadeLocal fileFacade;
	@ManagedProperty(value = "#{connectionController}")
	private ConnectionController connectionController;

	public AccountController() {
		super(Account.class);
	}

	@PostConstruct
	public void init() {
		student = new Student();
		if (connectionController.isConnected()) {
			setAccount(connectionController.getAccount());
			if(connectionController.isStudent()) student = new Student(account);
		} else {
			account = new Account();
		}
	}

	public String getCurrentImageData() {
		try {
			return Base64Encoder.encode(account.getProfilePicture()
					.getContent());
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public void handleFileUpload(FileUploadEvent event) {
		if (account.getProfilePicture() == null) {
			account.setProfilePicture(new File(event.getFile().getFileName(),
					event.getFile()
							.getFileName()
							.substring(
									event.getFile().getFileName()
											.lastIndexOf(".") + 1,
									event.getFile().getFileName().length()),
					event.getFile().getContents()));
		} else {
			account.getProfilePicture().setName(event.getFile().getFileName());
			account.getProfilePicture()
					.setType(
							event.getFile()
									.getFileName()
									.substring(
											event.getFile().getFileName()
													.lastIndexOf(".") + 1,
											event.getFile().getFileName()
													.length() - 1));
			account.getProfilePicture().setContent(
					event.getFile().getContents());
		}

		JsfUtil.addSuccessMessage("Success. " + event.getFile().getFileName()
				+ " is uploaded.");
	}

	public boolean save() {
		try {
			Student student = new Student(account);

			if (student.getProfilePicture() != null)
				fileFacade.create(account.getProfilePicture());

			student.setPasswd(MD5.getMD5(student.getPasswd()));

			if (sexValue == 0)
				student.setSexType(true);
			else
				student.setSexType(false);

			student.setClasse(this.student.getClasse());

			studentFacade.create(student);

			JsfUtil.addSuccessMessage("Successful registration. Please check your inbox and confirm us your e-mail address.");
			reset();
			return true;
		} catch (Exception e) {
			JsfUtil.addErrorMessage(e, "Error");
			e.printStackTrace();
			return false;
		}

	}

	public String saveChanges() {
		if (oldPasswd != null && !oldPasswd.equals("")
				&& MD5.getMD5(oldPasswd).equalsIgnoreCase(account.getPasswd())
				&& confirmPassword.equals(passwd)) {
			account.setPasswd(MD5.getMD5(passwd));
			if (connectionController.isStudent()) {
				Student student = (Student) account;
				student.setClasse(this.student.getClasse());
				studentFacade.edit(student);
			}
			if (connectionController.isTeacher()) {
				teacherFacade.edit((Teacher) account);
			}
			if (account.getProfilePicture() != null) {
				if (account.getProfilePicture().getId() != 0)
					fileFacade.edit(account.getProfilePicture());
				else
					fileFacade.create(account.getProfilePicture());
			}
			JsfUtil.addSuccessMessage("Changes saved.");
			return "/index";
		} else if (oldPasswd == null || oldPasswd.equals("")) {
			if (connectionController.isStudent())
				studentFacade.edit((Student) account);
			if (connectionController.isTeacher())
				teacherFacade.edit((Teacher) account);
			if (account.getProfilePicture() != null) {
				if (account.getProfilePicture().getId() != 0)
					fileFacade.edit(account.getProfilePicture());
				else
					fileFacade.create(account.getProfilePicture());
			}
			JsfUtil.addSuccessMessage("Changes saved.");
			return "/index";
		}
		JsfUtil.addErrorMessage("Not saved : Check your old password or password confirmation");
		return null;
	}

	public void reset() {
		try {
			account = new Account();
			student = new Student();
		} catch (Exception e) {
			JsfUtil.addErrorMessage(e, "Error");
		}

	}

	public String onFlowProcess(FlowEvent event) {
		if (event.getOldStep().equalsIgnoreCase("end")
				&& event.getNewStep().equalsIgnoreCase("confirm")) {
			return "personal";
		} else if (event.getNewStep().equalsIgnoreCase("confirm")) {
			if (account.getPasswd().equals(confirmPassword))
				return event.getNewStep();
			else {
				JsfUtil.addErrorMessage("Bad Password confirmation!");
				return event.getOldStep();
			}
		} else if (event.getNewStep().equalsIgnoreCase("end")) {
			if (!save())
				return event.getOldStep();
		}
		return event.getNewStep();
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public int getSexValue() {
		return sexValue;
	}

	public void setSexValue(int sexValue) {
		this.sexValue = sexValue;
		if (sexValue == 0)
			account.setSexType(true);
		else
			account.setSexType(false);
	}

	@Override
	public AbstractFacadeLocal<Account> getFacade() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Account> getData() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void prepareCreate(ActionEvent event) {
		// TODO Auto-generated method stub

	}

	@Override
	public void prepareDataForStudent() {
	}

	@Override
	public void prepareDataForTeacher() {
	}

	@Override
	public void prepareDataForAdmin() {
	}

	@Override
	public boolean canEdit() {
		if (getSelectedItem() != null
				&& getSelectedItem().equals(connectionController.getAccount()))
			return true;
		return false;
	}

	@Override
	public boolean canDelete() {
		if (getSelectedItem() != null
				&& getSelectedItem().equals(connectionController.getAccount()))
			return true;
		return false;
	}

	public void setConnectionController(
			ConnectionController connectionController) {
		this.connectionController = connectionController;
	}

	@Override
	public ConnectionController getConnectionController() {
		return connectionController;
	}

	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}

	public String getPasswd() {
		return passwd;
	}

	public String getOldPasswd() {
		return oldPasswd;
	}

	public void setOldPasswd(String oldPasswd) {
		this.oldPasswd = oldPasswd;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

}
