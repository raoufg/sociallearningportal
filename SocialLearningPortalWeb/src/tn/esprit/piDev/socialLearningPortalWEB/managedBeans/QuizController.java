package tn.esprit.piDev.socialLearningPortalWEB.managedBeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.Assessement;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Quiz;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Teacher;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.AbstractFacadeLocal;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.AssessementFacadeLocal;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.DocumentFacadeLocal;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.QuizFacadeLocal;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.StudentFacadeLocal;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.TeacherFacadeLocal;

@ManagedBean(name = "quizController")
@ViewScoped
public class QuizController extends AbstractController<Quiz> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	@EJB
	private QuizFacadeLocal facade;
	
	@ManagedProperty(value = "#{connectionController}")
    private ConnectionController connectionController;
	
    private Assessement selectedAssessement;
	
	@EJB
	private DocumentFacadeLocal documentFacade;
	@EJB 
	private TeacherFacadeLocal teacherFacade;
	@EJB
	private StudentFacadeLocal studentFacade;
	@EJB
	private AssessementFacadeLocal assessementFacade;

	public QuizController() {
		super(Quiz.class);
	}

	
	@Override
	public AbstractFacadeLocal<Quiz> getFacade() {
		return facade;
	}
	
	public void setConnectionController(
			ConnectionController connectionController) {
		this.connectionController = connectionController;
	}
	
	@Override
	public ConnectionController getConnectionController() {
		return connectionController;
	}
	
	@Override
	public void prepareCreate(ActionEvent event) {
		Quiz quiz = new Quiz();
		List<Assessement> assessements = new ArrayList<Assessement>();
		assessements.add(selectedAssessement);
		quiz.setAssessements(assessements);
		setNewItem(quiz);
	}
	@Override
	public void prepareDataForStudent() {
		// TODO Auto-generated method stub
	}
	@Override
	public void prepareDataForTeacher() {
		setData(facade.findByCreatorOrPublicQuiz((Teacher) connectionController.getAccount(), true));
		recreateModel();
	}
	@Override
	public void prepareDataForAdmin() {
		// TODO Auto-generated method stub
	}
	@Override
	public boolean canEdit() {
		if(getSelectedItem()!=null && getSelectedItem().getCreator() != null &&
				getSelectedItem().getCreator().equals((Teacher)connectionController.getAccount())) return true;
		return false;
	}
	@Override
	public boolean canDelete() {
		if(getSelectedItem()!=null && getSelectedItem().getCreator() != null &&
				getSelectedItem().getCreator().equals((Teacher)connectionController.getAccount())) return true;
		return false;
	}
	
	public void setSelectedAssessement(Assessement selectedAssessement) {
		this.selectedAssessement = selectedAssessement;
	}
	
	public Assessement getSelectedAssessement() {
		return selectedAssessement;
	}
	

}
