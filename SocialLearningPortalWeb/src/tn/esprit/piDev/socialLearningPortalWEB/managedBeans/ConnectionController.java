package tn.esprit.piDev.socialLearningPortalWEB.managedBeans;

import java.io.IOException;
import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.jboss.security.Base64Encoder;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.Account;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Admin;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Student;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Teacher;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.services.interfaces.local.UsersManagementLocal;
import tn.esprit.piDev.socialLearningPortalWEB.utils.MD5;
import tn.esprit.piDev.socialLearningPortalWEB.utils.faces.JsfUtil;

@ManagedBean(name = "connectionController")
@SessionScoped
public class ConnectionController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */

	private boolean connected = false;
	private boolean admin = false;
	private boolean student = false;
	private boolean teacher = false;
	private String username;
	private String password;
	private Account account;
	@ManagedProperty(value = "#{globalApplicationController}")
    private GlobalApplicationController applicationController;
	@EJB
	private UsersManagementLocal usersManagement;
	
	//visual
	private int rightMenuIndex=0;
	private boolean rightMenuVisible = false;

	public ConnectionController() {
		
	}
	
	public String disconnect(){
		connected = false;
		admin = false;
		student = false;
		teacher = false;
		password = null;
		account = null;
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("accountController", null); 
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("teacherCourseController", null); 
		System.gc();
		return "/index";
	}

	public String connect() {
		if(username==null || username.equals("") || password==null || password.equals("")) {
			JsfUtil.addErrorMessage("Invalid input for username or password!");
			return "/index";
		}
		account = usersManagement.authenticate(username, MD5.getMD5(password));
		if(account == null) {
			JsfUtil.addErrorMessage("Incorrect input for username or password!");
			return "/index";
		}
		if(account instanceof Admin) admin = true;
		if(account instanceof Teacher) teacher = true;
		if(account instanceof Student) student = true;
		password = "";
		connected = true;
		return "/index";
	}
	
	public String getCurrentImageData(){
		try {
			return Base64Encoder.encode(account.getProfilePicture().getContent());
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public boolean isConnected() {
		return connected;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isAdmin() {
		return admin;
	}
	
	public boolean isStudent() {
		return student;
	}
	
	public boolean isTeacher() {
		return teacher;
	}

	
	public Account getAccount() {
		return account;
	}

	public void setApplicationController(
			GlobalApplicationController applicationController) {
		this.applicationController = applicationController;
	}

	public int getRightMenuIndex() {
		return rightMenuIndex;
	}

	public void setRightMenuIndex(int rightMenuIndex) {
		this.rightMenuIndex = rightMenuIndex;
	}

	public boolean isRightMenuVisible() {
		return rightMenuVisible;
	}

	public void setRightMenuVisible(boolean rightMenuVisible) {
		this.rightMenuVisible = rightMenuVisible;
	}

}
