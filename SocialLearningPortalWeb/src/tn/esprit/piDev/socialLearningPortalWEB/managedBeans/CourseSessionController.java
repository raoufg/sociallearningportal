package tn.esprit.piDev.socialLearningPortalWEB.managedBeans;

import java.io.Serializable;
import java.util.Map;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.Course;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.CourseSession;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Student;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Teacher;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.AbstractFacadeLocal;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.AssessementFacadeLocal;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.CourseFacadeLocal;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.CourseSessionFacadeLocal;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.DocumentFacadeLocal;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.StudentFacadeLocal;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.TeacherFacadeLocal;

@ManagedBean(name = "courseSessionController")
@ViewScoped
public class CourseSessionController extends AbstractController<CourseSession> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	@EJB
	private CourseSessionFacadeLocal facade;
	
	@ManagedProperty(value = "#{connectionController}")
    private ConnectionController connectionController;
	
	@EJB
	private DocumentFacadeLocal documentFacade;
	@EJB 
	private TeacherFacadeLocal teacherFacade;
	@EJB
	private StudentFacadeLocal studentFacade;
	@EJB
	private AssessementFacadeLocal assessementFacade;
	@EJB
	private CourseFacadeLocal courseFacade;

	public CourseSessionController() {
		super(CourseSession.class);
	}
	
	@Override
	public AbstractFacadeLocal<CourseSession> getFacade() {
		return facade;
	}
	
	public void showAllCourseSession(ActionEvent event){
		setData(facade.findAll());
		recreateModel();
	}
	
	public void setConnectionController(
			ConnectionController connectionController) {
		this.connectionController = connectionController;
	}
	
	@Override
	public ConnectionController getConnectionController() {
		return connectionController;
	}
	
	@Override
	public void prepareCreate(ActionEvent event) {
		CourseSession courseSession = new CourseSession();
		courseSession.setCreator(connectionController.getAccount());
		setNewItem(courseSession);
	}
	@Override
	public void prepareDataForStudent() {
		setData(facade.findByStudent((Student) connectionController.getAccount()));
		recreateModel();
	}
	@Override
	public void prepareDataForTeacher() {
		CourseSession newItem = new CourseSession();
		
		Map<String, String> requestMap = FacesContext.getCurrentInstance()
                .getExternalContext().getRequestParameterMap();
		String param = (String) requestMap.get("courseId");
		if(param!=null){
			int courseId = Integer.parseInt(param);
			Course course = courseFacade.find(courseId);
			setData(facade.findByCourse(course));
			
			newItem.setCourse(course);
			recreateModel();
		}else{
			setData(facade.findByCreator((Teacher) connectionController.getAccount()));
			recreateModel();
		}
		
		setNewItem(newItem);
	}
	@Override
	public void prepareDataForAdmin() {
		// TODO Auto-generated method stub
	}
	@Override
	public boolean canEdit() {
		if(getSelectedItem()!=null && getSelectedItem().getCreator() != null &&
				getSelectedItem().getCreator().equals(connectionController.getAccount())) return true;
		return false;
	}
	@Override
	public boolean canDelete() {
		if(getSelectedItem()!=null && getSelectedItem().getCreator() != null &&
				getSelectedItem().getCreator().equals(connectionController.getAccount())) return true;
		return false;
	}
	
	public String showDocuments(){
		return "/pages/document";
	}
	
	public String showStudents(){
		return "/pages/student";
	}
	
	public String showAssessements(){
		return "/pages/assessement";
	}
	
	public String showTeachers(){
		return "/pages/teacher";
	}

}
