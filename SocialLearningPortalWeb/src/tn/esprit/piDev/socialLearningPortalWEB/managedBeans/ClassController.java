package tn.esprit.piDev.socialLearningPortalWEB.managedBeans;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.Class;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.AbstractFacadeLocal;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.ClassFacadeLocal;
import tn.esprit.piDev.socialLearningPortalWEB.managedBeans.AbstractController;
import tn.esprit.piDev.socialLearningPortalWEB.managedBeans.ConnectionController;

@ManagedBean(name = "classController")
@ViewScoped
public class ClassController extends AbstractController<Class> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	@EJB
	private ClassFacadeLocal facade;
	@ManagedProperty(value = "#{connectionController}")
    private ConnectionController connectionController;

	public ClassController() {
		super(Class.class);
	}
	
	
	@Override
	public AbstractFacadeLocal<Class> getFacade() {
		return facade;
	}
	@Override
	public List<Class> getData() {
		return facade.findAll();
	}
	
	public void setConnectionController(
			ConnectionController connectionController) {
		this.connectionController = connectionController;
	}
	
	@Override
	public ConnectionController getConnectionController() {
		return connectionController;
	}
	
	@Override
	public void prepareCreate(ActionEvent event) {
		Class class1 = new Class();
		setNewItem(class1);
	}
	@Override
	public void prepareDataForStudent() {
		// TODO Auto-generated method stub
	}
	@Override
	public void prepareDataForTeacher() {
		// TODO Auto-generated method stub
	}
	@Override
	public void prepareDataForAdmin() {
		// TODO Auto-generated method stub
	}
	@Override
	public boolean canEdit() {
		if(connectionController.isAdmin()) return true;
		return false;
	}
	@Override
	public boolean canDelete() {
		if(connectionController.isAdmin()) return true;
		return false;
	}
}
