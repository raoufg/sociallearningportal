package tn.esprit.piDev.socialLearningPortalWEB.managedBeans;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.Document;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.AbstractFacadeLocal;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.DocumentFacadeLocal;
import tn.esprit.piDev.socialLearningPortalWEB.managedBeans.AbstractController;
import tn.esprit.piDev.socialLearningPortalWEB.managedBeans.ConnectionController;

@ManagedBean(name = "documentController")
@ViewScoped
public class DocumentController extends AbstractController<Document> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	@EJB
	private DocumentFacadeLocal facade;
	@ManagedProperty(value = "#{connectionController}")
    private ConnectionController connectionController;

	public DocumentController() {
		super(Document.class);
	}
	
	
	@Override
	public AbstractFacadeLocal<Document> getFacade() {
		return facade;
	}
	@Override
	public List<Document> getData() {
		return facade.findByGroup(null);
	}
	
	public void setConnectionController(
			ConnectionController connectionController) {
		this.connectionController = connectionController;
	}
	
	@Override
	public ConnectionController getConnectionController() {
		return connectionController;
	}
	
	@Override
	public void prepareCreate(ActionEvent event) {
		Document document = new Document();
		document.setOwner(connectionController.getAccount());
		setNewItem(document);
	}
	@Override
	public void prepareDataForStudent() {
		// TODO Auto-generated method stub
	}
	@Override
	public void prepareDataForTeacher() {
		// TODO Auto-generated method stub
	}
	@Override
	public void prepareDataForAdmin() {
		// TODO Auto-generated method stub
	}
	@Override
	public boolean canEdit() {
		if(getSelectedItem()!=null && getSelectedItem().getOwner() != null &&
				getSelectedItem().getOwner().equals(connectionController.getAccount())) return true;
		return false;
	}
	@Override
	public boolean canDelete() {
		if(getSelectedItem()!=null && getSelectedItem().getOwner() != null &&
				getSelectedItem().getOwner().equals(connectionController.getAccount())) return true;
		return false;
	}
	

}
