package tn.esprit.piDev.socialLearningPortalWEB.managedBeans;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@ManagedBean(name = "globalApplicationController")
@ApplicationScoped
public class GlobalApplicationController {
	private ApplicationContext ctx;

	public GlobalApplicationController() {
		ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
	}

	public ApplicationContext getCtx() {
		return ctx;
	}
	
}
