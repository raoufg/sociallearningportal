package tn.esprit.piDev.socialLearningPortalWEB.utils;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.Assessement;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Class;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Course;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.CourseSession;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Quiz;
import tn.esprit.piDev.socialLearningPortalWEB.managedBeans.AssessementController;
import tn.esprit.piDev.socialLearningPortalWEB.managedBeans.ClassController;
import tn.esprit.piDev.socialLearningPortalWEB.managedBeans.CourseController;
import tn.esprit.piDev.socialLearningPortalWEB.managedBeans.CourseSessionController;
import tn.esprit.piDev.socialLearningPortalWEB.managedBeans.QuizController;

public class Converters {

	@ManagedBean(name="courseConverter")
	@RequestScoped
	@FacesConverter(forClass = Course.class, value="courseConverter")
	public static class CourseConverter implements Converter {

	    public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
	        if (value == null || value.length() == 0) {
	            return null;
	        }
	        CourseController controller = (CourseController) facesContext.getApplication().getELResolver().
	                    getValue(facesContext.getELContext(), null, "courseController");
	        return controller.getFacade().find(Integer.parseInt(value));
	    }

	    public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
	        if (object == null) {
	            return null;
	        }
	        if (object instanceof Course) {
	        	Course o = (Course) object;
	            return String.valueOf(o.getId());
	        } else {
	            throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Course.class.getName());
	        }
	    }
	}
	
	
	

	@ManagedBean(name="classConverter")
	@RequestScoped
	@FacesConverter(forClass = Class.class, value="classConverter")
    public static class ClassConverter implements Converter {

        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            ClassController controller = (ClassController) facesContext.getApplication().getELResolver().
	                    getValue(facesContext.getELContext(), null, "classController");
            return controller.getFacade().find(Integer.parseInt(value));
        }

        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Class) {
            	Class o = (Class) object;
                return String.valueOf(o.getId());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Class.class.getName());
            }
        }
    }
	
	
	@ManagedBean(name="courseSessionConverter")
	@RequestScoped
	@FacesConverter(forClass = CourseSession.class, value="courseSessionConverter")
    public static class CourseSessionConverter implements Converter {

        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            CourseSessionController controller = (CourseSessionController) facesContext.getApplication().getELResolver().
	                    getValue(facesContext.getELContext(), null, "courseSessionController");
            return controller.getFacade().find(Integer.parseInt(value));
        }

        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof CourseSession) {
            	CourseSession o = (CourseSession) object;
                return String.valueOf(o.getId());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + CourseSession.class.getName());
            }
        }
    }
	
	@ManagedBean(name="assessementConverter")
	@RequestScoped
	@FacesConverter(forClass = Assessement.class, value="assessementConverter")
    public static class AssessementConverter implements Converter {

        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            AssessementController controller = (AssessementController) facesContext.getApplication().getELResolver().
	                    getValue(facesContext.getELContext(), null, "assessementController");
            return controller.getFacade().find(Integer.parseInt(value));
        }

        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Assessement) {
            	Assessement o = (Assessement) object;
                return String.valueOf(o.getId());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Assessement.class.getName());
            }
        }
    }
	
	@ManagedBean(name="quizConverter")
	@RequestScoped
	@FacesConverter(forClass = Quiz.class, value="quizConverter")
    public static class QuizConverter implements Converter {

        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            QuizController controller = (QuizController) facesContext.getApplication().getELResolver().
	                    getValue(facesContext.getELContext(), null, "quizController");
            return controller.getFacade().find(Integer.parseInt(value));
        }

        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Quiz) {
            	Quiz o = (Quiz) object;
                return String.valueOf(o.getId());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Quiz.class.getName());
            }
        }
    }
}
