package tn.esprit.piDev.socialLearningPortalWEB.utils.faces.prime;

import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

public abstract class PrimeDataModel<T> extends ListDataModel<T> implements SelectableDataModel<T> {

	public PrimeDataModel(List<T> data) {
		super(data);
	}

	
}
