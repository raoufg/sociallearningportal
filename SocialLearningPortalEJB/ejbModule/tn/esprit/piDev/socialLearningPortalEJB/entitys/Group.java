package tn.esprit.piDev.socialLearningPortalEJB.entitys;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.interfaces.IEntity;

/**
 * Entity implementation class for Entity: Groupe
 * 
 */
@Entity
@Inheritance(strategy=InheritanceType.JOINED)
@NamedQueries({
	@NamedQuery(name="Group.findByName", query="SELECT d FROM Group d WHERE d.name LIKE :name"),
	@NamedQuery(name="Group.findByNameOrType", query="SELECT d FROM Group d WHERE d.name LIKE :name OR d.type LIKE :type"),
	@NamedQuery(name="Group.findByNameOrTypeAndOpen", query="SELECT d FROM Group d WHERE (d.name LIKE :name OR d.type LIKE :type) AND d.open = :open"),
	@NamedQuery(name="Group.findByStudent", query="SELECT d FROM Group d WHERE :student MEMBER OF d.students"),
	@NamedQuery(name="Group.findByCreator", query="SELECT d FROM Group d WHERE d.creator = :creator")
})
public class Group implements IEntity,Serializable {
	
	private static final long serialVersionUID = 1L;
	
	
	@Id
	@SequenceGenerator(name = "group_seq", sequenceName="GROUP_SEQ", allocationSize=1)
	@GeneratedValue(generator = "group_seq")
	private int id;
	private String name;
	private String type;
	private boolean open = false;
	
	@ManyToMany(mappedBy="groups")
	private List<Student> students;
	
	@OneToMany(mappedBy="course")
	private List<Document> documents;
	
	@ManyToOne
	private Account creator;

	public Group() {
		super();
	}

	
	public Group(String name, String type, boolean open,
			List<Student> students, List<Document> documents, Account creator) {
		super();
		this.name = name;
		this.type = type;
		this.open = open;
		this.students = students;
		this.documents = documents;
		this.creator = creator;
	}



	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public void setStudents(List<Student> students) {
		this.students = students;
	}
	
	public List<Student> getStudents() {
		return students;
	}
	
	public List<Document> getDocuments() {
		return documents;
	}
	
	public void setDocuments(List<Document> documents) {
		this.documents = documents;
	}
	
	public Account getCreator() {
		return creator;
	}
	
	public void setCreator(Account creator) {
		this.creator = creator;
	}
	
	public boolean isOpen() {
		return open;
	}
	
	public void setOpen(boolean open) {
		this.open = open;
	}
	
	@Override
	public String toString() {
		return  name;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((creator == null) ? 0 : creator.hashCode());
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + (open ? 1231 : 1237);
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Group other = (Group) obj;
		if (creator == null) {
			if (other.creator != null)
				return false;
		} else if (!creator.equals(other.creator))
			return false;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (open != other.open)
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}
	
	
}
