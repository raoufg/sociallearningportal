package tn.esprit.piDev.socialLearningPortalEJB.entitys;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Teatcher_Account
 *
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Teacher.findByCourseSession", query="SELECT r FROM Teacher r WHERE :courseSession MEMBER OF r.courseSessions")
})
public class Teacher extends Account implements Serializable {

	private static final long serialVersionUID = 1L;

	@ManyToMany
	private List<CourseSession> courseSessions;
	
	@OneToMany(mappedBy="creator")
	private List<Course> coursesCreated;
	
	@OneToMany(mappedBy="creator")
	private List<Quiz> quizs;
	
	public Teacher() {
		super();
	}   
	
	  
	public Teacher(String username, String passwd, String lastName,
			String firstName, String address, String phoneNumber, String cinNumber,
			String emailAddress, Date birthDate, boolean sexType,
			File profilePicture) {
		super(username, passwd, lastName, firstName, address, phoneNumber,
				cinNumber, emailAddress, birthDate, sexType, profilePicture);
	}


	public List<CourseSession> getCourseSessions() {
		return courseSessions;
	}
	
	public void setCourseSessions(List<CourseSession> courseSessions) {
		this.courseSessions = courseSessions;
	}
	
	public List<Course> getCoursesCreated() {
		return coursesCreated;
	}
	
	public void setCoursesCreated(List<Course> coursesCreated) {
		this.coursesCreated = coursesCreated;
	}
	
	public List<Quiz> getQuizs() {
		return quizs;
	}
	
	public void setQuizs(List<Quiz> quizs) {
		this.quizs = quizs;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((courseSessions == null) ? 0 : courseSessions.hashCode());
		result = prime * result
				+ ((coursesCreated == null) ? 0 : coursesCreated.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Teacher){
			return (((Teacher) obj).getId()== getId());
		}
		return false;
	}
	
	
   
}
