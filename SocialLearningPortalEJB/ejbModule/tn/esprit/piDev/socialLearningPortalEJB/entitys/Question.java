package tn.esprit.piDev.socialLearningPortalEJB.entitys;

import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.interfaces.IEntity;

/**
 * Entity implementation class for Entity: Question
 *
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Question.findByQuiz", query="SELECT q FROM Question q WHERE :quiz MEMBER OF q.quizs"),
	@NamedQuery(name="Question.findByTheme", query="SELECT q FROM Question q WHERE q.theme LIKE :theme")
})
public class Question implements IEntity,Serializable {

	private static final long serialVersionUID = 1L;
	   
	@Id
	@SequenceGenerator(name = "question_seq", sequenceName="QUESTION_SEQ", allocationSize=1)
	@GeneratedValue(generator = "question_seq")
	private int id;
	private String questionText;
	private String theme;
	
	@OneToMany(mappedBy="question")
	private List<Response> responses;
	@ManyToMany(mappedBy="questions")
	private List<Quiz> quizs;

	public Question() {
		super();
	}   
	
	public Question(String questionText, String theme, List<Response> responses) {
		super();
		this.questionText = questionText;
		this.theme = theme;
		this.responses = responses;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}   
	public String getQuestionText() {
		return this.questionText;
	}

	public void setQuestionText(String questionText) {
		this.questionText = questionText;
	}
	
	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	public void setResponses(List<Response> responses) {
		this.responses = responses;
	}
	
	public List<Response> getResponses() {
		return responses;
	}
	
	public List<Quiz> getQuizs() {
		return quizs;
	}
	
	public void setQuizs(List<Quiz> quizs) {
		this.quizs = quizs;
	}
   
	@Override
	public String toString() {
		return "Q-"+id;
	}
}
