package tn.esprit.piDev.socialLearningPortalEJB.entitys;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.interfaces.IEntity;

/**
 * Entity implementation class for Entity: Class
 *
 */
@Entity
public class Class implements IEntity,Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name = "class_seq", sequenceName="CLASS_SEQ", allocationSize=1)
	@GeneratedValue(generator = "class_seq")
	private int id;
	private int classLevel;
	private String classSection;
	private int classNumber;
	
	public Class() {
		super();
	}

	public Class(int classLevel, String classSection, int classNumber) {
		super();
		this.classLevel = classLevel;
		this.classSection = classSection;
		this.classNumber = classNumber;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	public int getClassLevel() {
		return classLevel;
	}

	public void setClassLevel(int classLevel) {
		this.classLevel = classLevel;
	}

	public String getClassSection() {
		return classSection;
	}

	public void setClassSection(String classSection) {
		this.classSection = classSection;
	}

	public int getClassNumber() {
		return classNumber;
	}

	public void setClassNumber(int classNumber) {
		this.classNumber = classNumber;
	}

	@Override
	public String toString() {
		return  classLevel + " " + classSection + " " + classNumber;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + classLevel;
		result = prime * result + classNumber;
		result = prime * result
				+ ((classSection == null) ? 0 : classSection.hashCode());
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Class other = (Class) obj;
		if (classLevel != other.classLevel)
			return false;
		if (classNumber != other.classNumber)
			return false;
		if (classSection == null) {
			if (other.classSection != null)
				return false;
		} else if (!classSection.equals(other.classSection))
			return false;
		if (id != other.id)
			return false;
		return true;
	}   
	
	
	
	
}
