package tn.esprit.piDev.socialLearningPortalEJB.entitys;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.interfaces.IEntity;

/**
 * Entity implementation class for Entity: Assessement
 *
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Assessement.findByName", query="SELECT a FROM Assessement a WHERE a.name LIKE :name ORDER BY a.beginDate"),
	@NamedQuery(name="Assessement.findByBeginDate", query="SELECT a FROM Assessement a WHERE a.beginDate =:beginDate  ORDER BY a.beginDate"),
	@NamedQuery(name="Assessement.findByQuiz", query="SELECT a FROM Assessement a WHERE a.quiz =:quiz  ORDER BY a.beginDate"),
	@NamedQuery(name="Assessement.findByCourseSession", query="SELECT a FROM Assessement a WHERE a.courseSession =:courseSession  ORDER BY a.beginDate"),
	@NamedQuery(name="Assessement.findByStudentAssmt", query="SELECT a FROM Assessement a WHERE :studentAssmt MEMBER OF a.studentAsses  ORDER BY a.beginDate"),
	@NamedQuery(name="Assessement.findByTeacher", query="SELECT a FROM Assessement a WHERE :teacher MEMBER OF a.courseSession.teachers OR a.courseSession.creator =:teacher  ORDER BY a.beginDate")
})
public class Assessement implements IEntity,Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name = "assessement_seq", sequenceName="ASSESSEMENT_SEQ", allocationSize=1)
	@GeneratedValue(generator = "assessement_seq")
	private int id;
	private String name;
	private Date beginDate;
	private Date expirationDate;
	@Temporal(TemporalType.TIME)
	private Date duration;
	private boolean activated = false;
	
	@ManyToOne(optional=false)
	private Quiz quiz;
	
	@ManyToOne
	private CourseSession courseSession;
	
	@OneToMany(mappedBy="assessement")
	private List<StudentAssmt> studentAsses;


	public Assessement() {
		super();
	}   
	
	
	public Assessement(String name, Date beginDate, Date expirationDate, Date duration, Quiz quiz,
			CourseSession courseSession, List<StudentAssmt> studentAsses) {
		super();
		this.name = name;
		this.beginDate = beginDate;
		this.duration = duration;
		this.quiz = quiz;
		this.courseSession = courseSession;
		this.studentAsses = studentAsses;
		this.expirationDate = expirationDate;
	}



	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public void setQuiz(Quiz quiz) {
		this.quiz = quiz;
	}
	
	public Quiz getQuiz() {
		return quiz;
	}

	public void setCourseSession(CourseSession courseSession) {
		this.courseSession = courseSession;
	}
	
	public CourseSession getCourseSession() {
		return courseSession;
	}
	
	public List<StudentAssmt> getStudentAsses() {
		return studentAsses;
	}
	public void setStudentAsses(List<StudentAssmt> studentAsses) {
		this.studentAsses = studentAsses;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getBeginDate() {
		return beginDate;
	}
	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}
	public Date getDuration() {
		return duration;
	}
	public void setDuration(Date duration) {
		this.duration = duration;
	}
	
	public Date getExpirationDate() {
		return expirationDate;
	}


	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}


	public boolean isActivated() {
		return activated;
	}


	public void setActivated(boolean activated) {
		this.activated = activated;
	}


	@Override
	public String toString() {
		return name;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (activated ? 1231 : 1237);
		result = prime * result
				+ ((beginDate == null) ? 0 : beginDate.hashCode());
		result = prime * result
				+ ((courseSession == null) ? 0 : courseSession.hashCode());
		result = prime * result
				+ ((duration == null) ? 0 : duration.hashCode());
		result = prime * result
				+ ((expirationDate == null) ? 0 : expirationDate.hashCode());
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((quiz == null) ? 0 : quiz.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Assessement other = (Assessement) obj;
		if (activated != other.activated)
			return false;
		if (beginDate == null) {
			if (other.beginDate != null)
				return false;
		} else if (!beginDate.equals(other.beginDate))
			return false;
		if (courseSession == null) {
			if (other.courseSession != null)
				return false;
		} else if (!courseSession.equals(other.courseSession))
			return false;
		if (duration == null) {
			if (other.duration != null)
				return false;
		} else if (!duration.equals(other.duration))
			return false;
		if (expirationDate == null) {
			if (other.expirationDate != null)
				return false;
		} else if (!expirationDate.equals(other.expirationDate))
			return false;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (quiz == null) {
			if (other.quiz != null)
				return false;
		} else if (!quiz.equals(other.quiz))
			return false;
		return true;
	}
	
	
   
}
