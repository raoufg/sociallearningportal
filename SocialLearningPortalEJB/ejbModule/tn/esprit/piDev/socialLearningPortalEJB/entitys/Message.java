package tn.esprit.piDev.socialLearningPortalEJB.entitys;

import java.io.Serializable;
import java.lang.String;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.interfaces.IEntity;

/**
 * Entity implementation class for Entity: Message
 *
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Message.findByRead", query="SELECT r FROM Message r WHERE r.read =:read ORDER BY r.sentDate DESC"),
	@NamedQuery(name="Message.findBySenderAccount", query="SELECT r FROM Message r WHERE r.senderAccount =:senderAccount ORDER BY r.sentDate DESC"),
	@NamedQuery(name="Message.findByReceiverAccount", query="SELECT r FROM Message r WHERE :receiverAccount MEMBER OF r.receiverAccounts ORDER BY r.sentDate DESC")
})
public class Message implements IEntity, Serializable {


	private static final long serialVersionUID = 1L;
	   
	@Id
	@SequenceGenerator(name = "message_seq", sequenceName="MESSAGE_SEQ", allocationSize=1)
	@GeneratedValue(generator = "message_seq")
	private int id;
	private String msgText;
	@OneToOne
	private Document document;
	private Date sentDate;
	private boolean read;
	private Date readDate;
	
	@ManyToOne(optional=false,cascade=CascadeType.ALL)
	private Account senderAccount;
	
	@ManyToMany
	private List<Account> receiverAccounts;

	public Message() {
		super();
	}   
	
	public Message(String msgText, Document document, Date sentDate,
			Account senderAccount, List<Account> receiverAccounts) {
		super();
		this.msgText = msgText;
		this.document = document;
		this.sentDate = sentDate;
		this.senderAccount = senderAccount;
		this.receiverAccounts = receiverAccounts;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}   
	public String getMsgText() {
		return this.msgText;
	}

	public void setMsgText(String msgText) {
		this.msgText = msgText;
	}
	public Document getDocument() {
		return document;
	}
	public void setDocument(Document document) {
		this.document = document;
	}
	public Date getSentDate() {
		return sentDate;
	}
	public void setSentDate(Date sentDate) {
		this.sentDate = sentDate;
	}
	public boolean isRead() {
		return read;
	}
	public void setRead(boolean read) {
		this.read = read;
	}
	public Date getReadDate() {
		return readDate;
	}
	public void setReadDate(Date readDate) {
		this.readDate = readDate;
	}
	public Account getSenderAccount() {
		return senderAccount;
	}
	public void setSenderAccount(Account senderAccount) {
		this.senderAccount = senderAccount;
	}
	public List<Account> getReceiverAccounts() {
		return receiverAccounts;
	}
	public void setReceiverAccounts(List<Account> receiverAccounts) {
		this.receiverAccounts = receiverAccounts;
	}
	
	@Override
	public String toString() {
		return msgText;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((document == null) ? 0 : document.hashCode());
		result = prime * result + id;
		result = prime * result + ((msgText == null) ? 0 : msgText.hashCode());
		result = prime * result + (read ? 1231 : 1237);
		result = prime * result
				+ ((readDate == null) ? 0 : readDate.hashCode());
		result = prime * result
				+ ((senderAccount == null) ? 0 : senderAccount.hashCode());
		result = prime * result
				+ ((sentDate == null) ? 0 : sentDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Message other = (Message) obj;
		if (document == null) {
			if (other.document != null)
				return false;
		} else if (!document.equals(other.document))
			return false;
		if (id != other.id)
			return false;
		if (msgText == null) {
			if (other.msgText != null)
				return false;
		} else if (!msgText.equals(other.msgText))
			return false;
		if (read != other.read)
			return false;
		if (readDate == null) {
			if (other.readDate != null)
				return false;
		} else if (!readDate.equals(other.readDate))
			return false;
		if (senderAccount == null) {
			if (other.senderAccount != null)
				return false;
		} else if (!senderAccount.equals(other.senderAccount))
			return false;
		if (sentDate == null) {
			if (other.sentDate != null)
				return false;
		} else if (!sentDate.equals(other.sentDate))
			return false;
		return true;
	}
   
}
