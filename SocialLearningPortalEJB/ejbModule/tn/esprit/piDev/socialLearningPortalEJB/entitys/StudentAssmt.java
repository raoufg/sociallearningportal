package tn.esprit.piDev.socialLearningPortalEJB.entitys;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@NamedQueries({
	@NamedQuery(name="StudentAssmt.findByStudent", query="SELECT r FROM StudentAssmt r WHERE r.student = :student"),
	@NamedQuery(name="StudentAssmt.findByAssessement", query="SELECT r FROM StudentAssmt r WHERE r.assessement = :assessement"),
	@NamedQuery(name="StudentAssmt.findByAssessementPassed", query="SELECT r FROM StudentAssmt r WHERE r.assessementPassed = :assessementPassed"),
	@NamedQuery(name="StudentAssmt.findByPassingAssessementNow", query="SELECT r FROM StudentAssmt r WHERE r.passingAssessementNow = :passingAssessementNow")
})
public class StudentAssmt implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	@EmbeddedId
	private StudentAssmtPK id;
	
	@ManyToOne(optional=false)
	@JoinColumn(name="idStudent", referencedColumnName="id", insertable=false, nullable=false, updatable=false)
	private Student student;
	
	@ManyToOne(optional=false)
	@JoinColumn(name="idAssessement", referencedColumnName="id", insertable=false, nullable=false, updatable=false)
	private Assessement assessement;
	
	@ManyToMany(mappedBy="studentAssmts")
	private List<Response> responses;
	
	private boolean assessementPassed=false;
	private boolean passingAssessementNow=false;
	
	private Date dateOfAssessBegin;
	private Date dateOfAssessEnd;
	
	@Transient
	@Temporal(TemporalType.TIME)
	private Date timeSinceAssessBegin;
	@Transient
	private int score;
	
	public StudentAssmt() {
	}

	

	public StudentAssmt(Student student, Assessement assessement) {
		super();
		this.student = student;
		this.assessement = assessement;
	}



	public StudentAssmtPK getId() {
		return id;
	}

	public void setId(StudentAssmtPK id) {
		this.id = id;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public Assessement getAssessement() {
		return assessement;
	}

	public void setAssessement(Assessement assessement) {
		this.assessement = assessement;
	}

	public boolean isAssessementPassed() {
		return assessementPassed;
	}

	public void setAssessementPassed(boolean assessementPassed) {
		this.assessementPassed = assessementPassed;
	}

	public boolean isPassingAssessementNow() {
		return passingAssessementNow;
	}

	public void setPassingAssessementNow(boolean passingAssessementNow) {
		this.passingAssessementNow = passingAssessementNow;
	}

	public Date getTimeSinceAssessBegin() {
		return timeSinceAssessBegin;
	}

	public void setTimeSinceAssessBegin(Date timeSinceAssessBegin) {
		this.timeSinceAssessBegin = timeSinceAssessBegin;
	}

	public Date getDateOfAssessBegin() {
		return dateOfAssessBegin;
	}

	public void setDateOfAssessBegin(Date dateOfAssessBegin) {
		this.dateOfAssessBegin = dateOfAssessBegin;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public List<Response> getResponses() {
		return responses;
	}
	
	public void setResponses(List<Response> responses) {
		this.responses = responses;
	}
	
	public Date getDateOfAssessEnd() {
		return dateOfAssessEnd;
	}
	
	public void setDateOfAssessEnd(Date dateOfAssessEnd) {
		this.dateOfAssessEnd = dateOfAssessEnd;
	}
	
	@Override
	public String toString() {
		return student + " : " +assessement ;
	}
	
}
