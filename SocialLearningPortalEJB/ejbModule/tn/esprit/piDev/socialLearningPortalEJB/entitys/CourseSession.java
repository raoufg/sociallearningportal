package tn.esprit.piDev.socialLearningPortalEJB.entitys;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: CourseSession
 *
 */

@Entity
@NamedQueries({
	@NamedQuery(name="CourseSession.findByCourse", query="SELECT a FROM CourseSession a WHERE a.course = :course ORDER BY a.beginDate DESC"),
	@NamedQuery(name="CourseSession.findByTeacher", query="SELECT a FROM CourseSession a WHERE :teacher MEMBER OF a.teachers ORDER BY a.beginDate DESC"),
	@NamedQuery(name="CourseSession.findByStudent", query="SELECT a FROM CourseSession a WHERE :student MEMBER OF a.students ORDER BY a.beginDate DESC"),
	@NamedQuery(name="CourseSession.findByCreator", query="SELECT a FROM CourseSession a WHERE a.creator =:creator ORDER BY a.beginDate DESC")
})
public class CourseSession extends Group implements Serializable {

	
	private static final long serialVersionUID = 1L;

	private Date beginDate;
	private Date endDate;
	
	@ManyToOne
	private Course course;

	@OneToMany(mappedBy="courseSession")
	private List<Assessement>assessements;
	
	@ManyToMany(mappedBy="courseSessions")
	private List<Teacher> teachers;
	
	
	public CourseSession() {
		super();
	}
	

	public CourseSession(String name, String type, boolean open,
			List<Student> students, List<Document> documents, Account creator,
			Course course, Date beginDate, Date endDate, List<Teacher> teachers) {
		super(name, type, open, students, documents, creator);
		this.course = course;
		this.beginDate = beginDate;
		this.endDate = endDate;
		this.teachers = teachers;
	}


	public Course getCourse() {
		return course;
	}
	
	public void setCourse(Course course) {
		this.course = course;
	}

	public Date getBeginDate() {
		return beginDate;
	}


	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}


	public Date getEndDate() {
		return endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	public List<Teacher> getTeachers() {
		return teachers;
	}
	
	public void setTeachers(List<Teacher> teachers) {
		this.teachers = teachers;
	}

   
}
