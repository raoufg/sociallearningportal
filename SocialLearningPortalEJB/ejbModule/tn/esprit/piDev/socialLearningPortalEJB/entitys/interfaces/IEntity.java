package tn.esprit.piDev.socialLearningPortalEJB.entitys.interfaces;

public interface IEntity {
	public int getId();
}
