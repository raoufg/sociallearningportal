package tn.esprit.piDev.socialLearningPortalEJB.entitys;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.interfaces.IEntity;

/**
 * Entity implementation class for Entity: Document
 *
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Document.findByDescription", query="SELECT d FROM Document d WHERE d.description LIKE :description"),
	@NamedQuery(name="Document.findByCourse", query="SELECT d FROM Document d WHERE d.course = :course"),
	@NamedQuery(name="Document.findByGroup", query="SELECT d FROM Document d WHERE d.group = :group")
})
public class Document implements IEntity,Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "document_seq", sequenceName="DOCUMENT_SEQ", allocationSize=1)
	@GeneratedValue(generator = "document_seq")
	private int id;
	private String description;
	@OneToOne
	private File file;
	@ManyToOne
	private Account owner;
	@ManyToOne
	private Course course;
	@ManyToOne
	private Group group;
	
	
	public Document() {
	}   
	
	
	    
	public Document(String description, File file, Account account) {
		super();
		this.description = description;
		this.file = file;
		this.owner = account;
	}



	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}   
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public Account getOwner() {
		return owner;
	}



	public void setOwner(Account owner) {
		this.owner = owner;
	}



	public Course getCourse() {
		return course;
	}
	public void setCourse(Course course) {
		this.course = course;
	}
	
	public Group getGroup() {
		return group;
	}
	
	public void setGroup(Group group) {
		this.group = group;
	}
	
	public File getFile() {
		return file;
	}
	
	public void setFile(File file) {
		this.file = file;
	}
	
	@Override
	public String toString() {
		return  file.getName();
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((course == null) ? 0 : course.hashCode());
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((file == null) ? 0 : file.hashCode());
		result = prime * result + ((group == null) ? 0 : group.hashCode());
		result = prime * result + id;
		result = prime * result + ((owner == null) ? 0 : owner.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Document other = (Document) obj;
		if (course == null) {
			if (other.course != null)
				return false;
		} else if (!course.equals(other.course))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (file == null) {
			if (other.file != null)
				return false;
		} else if (!file.equals(other.file))
			return false;
		if (group == null) {
			if (other.group != null)
				return false;
		} else if (!group.equals(other.group))
			return false;
		if (id != other.id)
			return false;
		if (owner == null) {
			if (other.owner != null)
				return false;
		} else if (!owner.equals(other.owner))
			return false;
		return true;
	}
   
}
