package tn.esprit.piDev.socialLearningPortalEJB.entitys;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.interfaces.IEntity;

/**
 * Entity implementation class for Entity: Post
 *
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Post.findByReplyTo", query="SELECT r FROM Post r WHERE r.replyTo =:replyTo ORDER BY r.postDate DESC"),
	@NamedQuery(name="Post.findByGroup", query="SELECT r FROM Post r WHERE r.group =:group ORDER BY r.postDate DESC")
})
public class Post implements IEntity, Serializable {

	private static final long serialVersionUID = 1L;
	   
	@Id
	@SequenceGenerator(name = "post_seq", sequenceName="POST_SEQ", allocationSize=1)
	@GeneratedValue(generator = "post_seq")
	private int id;
	private String postText;
	private Date postDate;
	@OneToOne
	private Document document;
	@ManyToOne(optional=false,cascade=CascadeType.ALL)
	private Account account;
	@ManyToOne(optional=false,cascade=CascadeType.ALL)
	private Post replyTo;
	@ManyToOne(optional=false,cascade=CascadeType.ALL)
	private Group group;
	@OneToMany(mappedBy="replyTo")
	private List<Post> replies;

	public Post() {
		super();
	}   
	
	public Post(String postText, Date postDate, Document document,
			Account account, Post replyTo, Group group) {
		super();
		this.postText = postText;
		this.postDate = postDate;
		this.document = document;
		this.account = account;
		this.replyTo = replyTo;
		this.group = group;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}
	public String getPostText() {
		return postText;
	}
	public void setPostText(String postText) {
		this.postText = postText;
	}
	public Date getPostDate() {
		return postDate;
	}
	public void setPostDate(Date postDate) {
		this.postDate = postDate;
	}
	public Document getDocument() {
		return document;
	}
	public void setDocument(Document document) {
		this.document = document;
	}
	public Account getAccount() {
		return account;
	}
	public void setAccount(Account account) {
		this.account = account;
	}
	public Post getReplyTo() {
		return replyTo;
	}
	public void setReplyTo(Post replyTo) {
		this.replyTo = replyTo;
	}
	public Group getGroup() {
		return group;
	}
	public void setGroup(Group group) {
		this.group = group;
	}
	
	public List<Post> getReplies() {
		return replies;
	}
	
	public void setReplies(List<Post> replies) {
		this.replies = replies;
	}
	
	@Override
	public String toString() {
		return postText;
	}
   
}
