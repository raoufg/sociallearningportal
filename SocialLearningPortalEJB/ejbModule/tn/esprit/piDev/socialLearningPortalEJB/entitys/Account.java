package tn.esprit.piDev.socialLearningPortalEJB.entitys;

import java.io.Serializable;
import java.lang.String;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.interfaces.IEntity;

/**
 * Entity implementation class for Entity: Account
 *
 */
@Entity
@Inheritance(strategy=InheritanceType.JOINED)
@NamedQueries({
	@NamedQuery(name="Account.findByUsername", query="SELECT a FROM Account a WHERE a.username LIKE :username"),
	@NamedQuery(name="Account.findByUsernameAndPasswd", query="SELECT a FROM Account a WHERE a.username =:username AND a.passwd = :passwd"),
	@NamedQuery(name="Account.findByLastNameOrFirstName", query="SELECT a FROM Account a WHERE a.lastName like :lastName OR a.firstName like :firstName"),
	@NamedQuery(name="Account.findByMessageReceived", query="SELECT a FROM Account a WHERE :message MEMBER OF a.messagesReceived")
})
public class Account implements IEntity, Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name = "account_seq", sequenceName="ACCOUNT_SEQ", allocationSize=1)
	@GeneratedValue(generator = "account_seq")
	private int id;
	@Column(unique=true,nullable=false)
	private String username;
	@Column(nullable=false)
	private String passwd;
	@Column(nullable=true)
	private String lastName;
	@Column(nullable=true)
	private String firstName;
	@Column(nullable=true)
	private String address;
	@Column(nullable=true)
	private String phoneNumber;
	@Column(unique=true,nullable=true)
	private String cinNumber;
	@Column(unique=true,nullable=false)
	private String emailAddress;
	@Column(nullable=true)
	private Date birthDate;
	@Column(nullable=true)
	private boolean sexType;
	@OneToOne
	private File profilePicture;
	
	@OneToMany(mappedBy="creator")
	private List<Group> groupsCreated;
	
	@OneToMany(mappedBy="senderAccount")
	private List<Message> messagesSent;
	
	@ManyToMany(mappedBy="receiverAccounts")
	private List<Message> messagesReceived;
	
	@OneToMany(mappedBy="account")
	private List<Post> posts;
	
	@OneToMany(mappedBy="owner")
	private List<Document> documents;
	
	public Account() {
	}
	
	

	public Account(String username, String passwd, String lastName,
			String firstName, String address, String phoneNumber, String cinNumber,
			String emailAddress, Date birthDate, boolean sexType,
			File profilePicture) {
		super();
		this.username = username;
		this.passwd = passwd;
		this.lastName = lastName;
		this.firstName = firstName;
		this.address = address;
		this.phoneNumber = phoneNumber;
		this.cinNumber = cinNumber;
		this.emailAddress = emailAddress;
		this.birthDate = birthDate;
		this.sexType = sexType;
		this.profilePicture = profilePicture;
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getUsername() {
		return username;
	}



	public void setUsername(String username) {
		this.username = username;
	}



	public String getPasswd() {
		return passwd;
	}



	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}



	public String getLastName() {
		return lastName;
	}



	public void setLastName(String lastName) {
		this.lastName = lastName;
	}



	public String getFirstName() {
		return firstName;
	}



	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}



	public String getAddress() {
		return address;
	}



	public void setAddress(String address) {
		this.address = address;
	}



	public String getPhoneNumber() {
		return phoneNumber;
	}



	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}



	public String getCinNumber() {
		return cinNumber;
	}



	public void setCinNumber(String cinNumber) {
		this.cinNumber = cinNumber;
	}



	public String getEmailAddress() {
		return emailAddress;
	}



	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}



	public Date getBirthDate() {
		return birthDate;
	}



	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}



	public boolean isSexType() {
		return sexType;
	}



	public void setSexType(boolean sexType) {
		this.sexType = sexType;
	}

	public File getProfilePicture() {
		return profilePicture;
	}

	public void setProfilePicture(File profilePicture) {
		this.profilePicture = profilePicture;
	}




	public void setGroupsCreated(List<Group> groupsCreated) {
		this.groupsCreated = groupsCreated;
	}





	public void setMessagesSent(List<Message> messagesSent) {
		this.messagesSent = messagesSent;
	}





	public void setMessagesReceived(List<Message> messagesReceived) {
		this.messagesReceived = messagesReceived;
	}






	public void setPosts(List<Post> posts) {
		this.posts = posts;
	}
	
	
	public void setDocuments(List<Document> documents) {
		this.documents = documents;
	}
	
	


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result
				+ ((birthDate == null) ? 0 : birthDate.hashCode());
		result = prime * result
				+ ((cinNumber == null) ? 0 : cinNumber.hashCode());
		result = prime * result
				+ ((emailAddress == null) ? 0 : emailAddress.hashCode());
		result = prime * result
				+ ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + id;
		result = prime * result
				+ ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result
				+ ((phoneNumber == null) ? 0 : phoneNumber.hashCode());
		result = prime * result
				+ ((username == null) ? 0 : username.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Account other = (Account) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (birthDate == null) {
			if (other.birthDate != null)
				return false;
		} else if (!birthDate.equals(other.birthDate))
			return false;
		if (cinNumber == null) {
			if (other.cinNumber != null)
				return false;
		} else if (!cinNumber.equals(other.cinNumber))
			return false;
		if (emailAddress == null) {
			if (other.emailAddress != null)
				return false;
		} else if (!emailAddress.equals(other.emailAddress))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (id != other.id)
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (phoneNumber == null) {
			if (other.phoneNumber != null)
				return false;
		} else if (!phoneNumber.equals(other.phoneNumber))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}



	@Override
	public String toString() {
		return firstName+" "+lastName;
	}
    
}
