package tn.esprit.piDev.socialLearningPortalEJB.entitys;

import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.interfaces.IEntity;

/**
 * Entity implementation class for Entity: Quiz
 *
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Quiz.findByName", query="SELECT q FROM Quiz q WHERE q.name LIKE :name"),
	@NamedQuery(name="Quiz.findByQuestion", query="SELECT q FROM Quiz q WHERE :question MEMBER OF q.questions"),
	@NamedQuery(name="Quiz.findByCreator", query="SELECT q FROM Quiz q WHERE q.creator = :creator"),
	@NamedQuery(name="Quiz.findByCreatorOrPublicQuiz", query="SELECT q FROM Quiz q WHERE q.creator = :creator OR q.publicQuiz =:publicQuiz")
})
public class Quiz implements IEntity,Serializable {

	
	private static final long serialVersionUID = 1L;
	
	
	@Id
	@SequenceGenerator(name = "quiz_seq", sequenceName="QUIZ_SEQ", allocationSize=1)
	@GeneratedValue(generator = "quiz_seq")
	private int id;
	private String name;
	private boolean publicQuiz;
	
	@ManyToOne
	private Teacher creator;
	
	@ManyToMany
	private List<Question> questions;
	
	@OneToMany(mappedBy="quiz")
	private List<Assessement> assessements;

	public Quiz() {
		super();
	}   
	
	public Quiz(String name, boolean publicQuiz, Teacher creator) {
		super();
		this.name = name;
		this.publicQuiz = publicQuiz;
		this.creator = creator;
	}



	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}   
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public List<Question> getQuestions() {
		return questions;
	}
	
	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}
	
	public List<Assessement> getAssessements() {
		return assessements;
	}
	
	public void setAssessements(List<Assessement> assessements) {
		this.assessements = assessements;
	}
	
	public Teacher getCreator() {
		return creator;
	}
	
	public void setCreator(Teacher creator) {
		this.creator = creator;
	}
	
	public boolean isPublicQuiz() {
		return publicQuiz;
	}

	public void setPublicQuiz(boolean publicQuiz) {
		this.publicQuiz = publicQuiz;
	}

	@Override
	public String toString() {
		return name;
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((creator == null) ? 0 : creator.hashCode());
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + (publicQuiz ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Quiz other = (Quiz) obj;
		if (creator == null) {
			if (other.creator != null)
				return false;
		} else if (!creator.equals(other.creator))
			return false;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (publicQuiz != other.publicQuiz)
			return false;
		return true;
	}

   
}
