package tn.esprit.piDev.socialLearningPortalEJB.entitys;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.interfaces.IEntity;

/**
 * Entity implementation class for Entity: Course
 *
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Course.findByName", query="SELECT a FROM Course a WHERE a.name LIKE :name"),
	@NamedQuery(name="Course.findBySubject", query="SELECT a FROM Course a WHERE a.subject LIKE :subject"),
	@NamedQuery(name="Course.findByCreator", query="SELECT a FROM Course a WHERE a.creator = :creator"),
	@NamedQuery(name="Course.findByDocument", query="SELECT a FROM Course a WHERE :document MEMBER OF a.documents")
})
public class Course implements IEntity,Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name = "course_seq", sequenceName="COURSE_SEQ", allocationSize=1, initialValue=1)
	@GeneratedValue(generator = "course_seq")
	private int id;
	private String name;
	private String subject;
	@ManyToOne
	private Teacher creator;
	@ManyToOne
	private Category category;
	
	@OneToMany(mappedBy="course")
	private List<Document> documents;
	
	@OneToMany(mappedBy="course")
	private List<CourseSession> courseSessions;
	
	

	public Course() {
		super();
	}
	
	

	public Course(String name, String subject, Teacher creator,
			List<Document> documents) {
		super();
		this.name = name;
		this.subject = subject;
		this.creator = creator;
		this.documents = documents;
	}



	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Document> getDocuments() {
		return documents;
	}

	public void setDocuments(List<Document> documents) {
		this.documents = documents;
	}
	
	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public Teacher getCreator() {
		return creator;
	}

	public void setCreator(Teacher creator) {
		this.creator = creator;
	}
	
	public List<CourseSession> getCourseSessions() {
		return courseSessions;
	}
	
	public void setCourseSessions(List<CourseSession> courseSessions) {
		this.courseSessions = courseSessions;
	}
	
	public Category getCategory() {
		return category;
	}
	
	public void setCategory(Category category) {
		this.category = category;
	}
	
	@Override
	public String toString() {
		return name;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((creator == null) ? 0 : creator.hashCode());
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((subject == null) ? 0 : subject.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Course other = (Course) obj;
		if (creator == null) {
			if (other.creator != null)
				return false;
		} else if (!creator.equals(other.creator))
			return false;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (subject == null) {
			if (other.subject != null)
				return false;
		} else if (!subject.equals(other.subject))
			return false;
		return true;
	}

	

}
