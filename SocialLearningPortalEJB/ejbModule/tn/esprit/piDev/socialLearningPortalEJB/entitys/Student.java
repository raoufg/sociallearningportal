package tn.esprit.piDev.socialLearningPortalEJB.entitys;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Student_Acount
 * 
 */
@Entity
@NamedQueries({
		@NamedQuery(name = "Student.findByClass", query = "SELECT r FROM Student r WHERE r.classe = :classe"),
		@NamedQuery(name = "Student.findByGroup", query = "SELECT r FROM Student r WHERE :group MEMBER OF r.groups") })
public class Student extends Account implements Serializable {

	private static final long serialVersionUID = 1L;
	@ManyToOne
	private Class classe;
	@ManyToMany
	private List<Group> groups;

	public Student() {
		super();
	}

	public Student(Account account) {
		super(account.getUsername(), account.getPasswd(),
				account.getLastName(), account.getFirstName(), account
						.getAddress(), account.getPhoneNumber(), account
						.getCinNumber(), account.getEmailAddress(), account
						.getBirthDate(), account.isSexType(), account
						.getProfilePicture());
	}

	public Student(String username, String passwd, String lastName,
			String firstName, String address, String phoneNumber,
			String cinNumber, String emailAddress, Date birthDate,
			boolean sexType, File profilePicture, Class classe) {
		super(username, passwd, lastName, firstName, address, phoneNumber,
				cinNumber, emailAddress, birthDate, sexType, profilePicture);
		this.classe = classe;
	}

	public void setClasse(Class classe) {
		this.classe = classe;
	}

	public Class getClasse() {
		return classe;
	}
}
