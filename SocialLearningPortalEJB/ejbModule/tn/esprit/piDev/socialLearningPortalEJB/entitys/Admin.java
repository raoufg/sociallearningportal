package tn.esprit.piDev.socialLearningPortalEJB.entitys;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.Account;

/**
 * Entity implementation class for Entity: Admin
 *
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Admin.findByAdminPrivileges", query="SELECT r FROM Admin r WHERE r.adminPrivileges = :adminPrivileges")
})
public class Admin extends Account implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	private String adminPrivileges;

	public Admin() {
		super();
	}

	
	
	public Admin(String username, String passwd, String lastName,
			String firstName, String address, String phoneNumber, String cinNumber,
			String emailAddress, Date birthDate, boolean sexType,
			File profilePicture, String adminPrivileges) {
		super(username, passwd, lastName, firstName, address, phoneNumber,
				cinNumber, emailAddress, birthDate, sexType, profilePicture);
		this.adminPrivileges = adminPrivileges;
	}



	public String getAdminPrivileges() {
		return adminPrivileges;
	}
	
	public void setAdminPrivileges(String adminPrivileges) {
		this.adminPrivileges = adminPrivileges;
	}



	@Override
	public String toString() {
		return "Admin " + super.toString();
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((adminPrivileges == null) ? 0 : adminPrivileges.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Admin other = (Admin) obj;
		if (adminPrivileges == null) {
			if (other.adminPrivileges != null)
				return false;
		} else if (!adminPrivileges.equals(other.adminPrivileges))
			return false;
		return true;
	}
	
	
	
	
   
}
