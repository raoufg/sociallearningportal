package tn.esprit.piDev.socialLearningPortalEJB.entitys;

import java.io.Serializable;
import java.lang.Boolean;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.Question;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.interfaces.IEntity;

/**
 * Entity implementation class for Entity: Response
 *
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Response.findByQuestion", query="SELECT r FROM Response r WHERE r.question =:question"),
	@NamedQuery(name="Response.findByStudentAssmt", query="SELECT r FROM Response r WHERE :studentAssmt MEMBER OF r.studentAssmts")
})
public class Response implements IEntity,Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name = "response_seq", sequenceName="RESPONSE_SEQ", allocationSize=1)
	@GeneratedValue(generator = "response_seq")
	private int id;
	private String responseText;
	private Boolean trueResponse;
	
	@ManyToOne(optional=false)
	private Question question;
	
	@ManyToMany
	private List<StudentAssmt> studentAssmts;
	

	public Response() {
		super();
	}   
	
	public Response(String responseText, Boolean trueResponse, Question question) {
		super();
		this.responseText = responseText;
		this.trueResponse = trueResponse;
		this.question = question;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}   
	public String getResponseText() {
		return this.responseText;
	}

	public void setResponseText(String responseText) {
		this.responseText = responseText;
	}   
	
	public Boolean getTrueResponse() {
		return this.trueResponse;
	}

	public void setTrueResponse(Boolean trueResponse) {
		this.trueResponse = trueResponse;
	}   
	public Question getQuestion() {
		return this.question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}
	
	public List<StudentAssmt> getStudentAssessements() {
		return studentAssmts;
	}
	
	public void setStudentAssessements(
			List<StudentAssmt> studentAssmts) {
		this.studentAssmts = studentAssmts;
	}
	
	
	@Override
	public String toString() {
		return responseText;
	}
   
}
