package tn.esprit.piDev.socialLearningPortalEJB.entitys;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class StudentAssmtPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int idStudent;
	private int idAssessement;
	
	public StudentAssmtPK() {
	}

	public StudentAssmtPK(int idStudent, int idAssessement) {
		super();
		this.idStudent = idStudent;
		this.idAssessement = idAssessement;
	}

	public int getIdStudent() {
		return idStudent;
	}

	public void setIdStudent(int idStudent) {
		this.idStudent = idStudent;
	}

	public int getIdAssessement() {
		return idAssessement;
	}

	public void setIdAssessement(int idAssessement) {
		this.idAssessement = idAssessement;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idAssessement;
		result = prime * result + idStudent;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StudentAssmtPK other = (StudentAssmtPK) obj;
		if (idAssessement != other.idAssessement)
			return false;
		if (idStudent != other.idStudent)
			return false;
		return true;
	}
	
	
}
