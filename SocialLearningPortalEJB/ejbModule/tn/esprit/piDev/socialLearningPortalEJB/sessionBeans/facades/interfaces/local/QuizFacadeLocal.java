package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.Question;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Quiz;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Teacher;


@Local
public interface QuizFacadeLocal extends AbstractFacadeLocal<Quiz> {
	public Question findQuizsForQuestion(Question question);
	public List<Quiz> findByName(String name);
	public List<Quiz> findByQuestion(Question question);
	public List<Quiz> findByCreator(Teacher teacher);
	public List<Quiz> findByCreatorOrPublicQuiz(Teacher teacher, boolean publicQuiz);
}
