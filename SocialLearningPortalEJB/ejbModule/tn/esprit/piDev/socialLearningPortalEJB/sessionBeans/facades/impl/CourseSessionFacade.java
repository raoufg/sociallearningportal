package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Course;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.CourseSession;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Student;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Teacher;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.CourseSessionFacadeLocal;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.remote.CourseSessionFacadeRemote;

/**
 * Session Bean implementation class CourseFacade
 */
@Stateless
@WebService(serviceName="CourseSessionFacadeWS", endpointInterface="tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.remote.CourseSessionFacadeRemote")
public class CourseSessionFacade extends AbstractFacade<CourseSession> implements CourseSessionFacadeRemote, CourseSessionFacadeLocal {

	@PersistenceContext(unitName="SocialLearningPortal")
	private EntityManager em;
	
	
    /**
     * Default constructor. 
     */
    
       
    public CourseSessionFacade() {
        super(CourseSession.class);
        
    }

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CourseSession> findByCourse(Course course) {
		try {
			return getEntityManager()
					.createNamedQuery("CourseSession.findByCourse")
					.setParameter("course", course)
					.getResultList();
		} catch (Exception e) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CourseSession> findByTeacher(Teacher teacher) {
		try {
			return getEntityManager()
					.createNamedQuery("CourseSession.findByTeacher")
					.setParameter("teacher", teacher)
					.getResultList();
		} catch (Exception e) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CourseSession> findByStudent(Student student) {
		try {
			return getEntityManager()
					.createNamedQuery("CourseSession.findByStudent")
					.setParameter("student", student)
					.getResultList();
		} catch (Exception e) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CourseSession> findByCreator(Teacher creator) {
		try {
			return getEntityManager()
					.createNamedQuery("CourseSession.findByCreator")
					.setParameter("creator", creator)
					.getResultList();
		} catch (Exception e) {
			return null;
		}
	}
	

}
