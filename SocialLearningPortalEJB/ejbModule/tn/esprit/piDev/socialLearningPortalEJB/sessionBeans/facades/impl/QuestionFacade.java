package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Question;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Quiz;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.QuestionFacadeLocal;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.remote.QuestionFacadeRemote;


/**
 * Session Bean implementation class QuestionFacade
 */
@Stateless
@WebService(serviceName="QuestionFacadeWS", endpointInterface="tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.remote.QuestionFacadeRemote")
public class QuestionFacade extends AbstractFacade<Question> implements
		QuestionFacadeLocal, QuestionFacadeRemote {

	@PersistenceContext(unitName = "SocialLearningPortal")
	private EntityManager em;

	/**
	 * Default constructor.
	 */
	public QuestionFacade() {
		super(Question.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	@SuppressWarnings("unchecked")
	public Quiz findQuestionsForQuiz(Quiz quiz) {
		try{
			quiz.setQuestions(getEntityManager()
				.createNamedQuery("Question.findByQuiz")
				.setParameter("quiz", quiz).getResultList());
			return quiz;
		}catch(Exception e){
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Question> findByQuiz(Quiz quiz) {
		try{
			return getEntityManager()
					.createNamedQuery("Question.findByQuiz")
					.setParameter("quiz", quiz)
					.getResultList();
		}catch(Exception e){
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Question> findByTheme(String theme) {
		try{
			return getEntityManager()
					.createNamedQuery("Question.findByTheme")
					.setParameter("theme", theme)
					.getResultList();
		}catch(Exception e){
			return null;
		}
	}

}
