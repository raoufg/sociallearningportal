package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.CourseSession;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Teacher;


@Local
public interface TeacherFacadeLocal extends AbstractFacadeLocal<Teacher> {

	List<Teacher> findByCourseSession(CourseSession courseSession);
}
