package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.remote;

import java.util.List;

import javax.ejb.Remote;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.Account;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Course;


@Remote
@Path("/WS-REST/CourseFacade")
@WebService(name="CourseFacadeWS")
public interface CourseFacadeRemote extends AbstractFacadeRemote<Course> {
	
	@GET
	@Path("/findByName/{name}/{account}")
	@Produces("application/xml")
	@WebMethod(operationName="findByName")
	public List<Course> findByName(@PathParam("name") String name, @PathParam("account") Account account);
	
}
