package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.remote;

import javax.ejb.Remote;
import javax.jws.WebService;
import javax.ws.rs.Path;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.StudentAssmt;


@Remote
@Path("/WS-REST/StudentAssmtFacade")
@WebService(name="StudentAssmtFacadeWS")
public interface StudentAssmtFacadeRemote extends AbstractFacadeRemote<StudentAssmt> {
	
}
