package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;
import javax.validation.ConstraintViolationException;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.interfaces.IEntity;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.AbstractFacadeLocal;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.remote.AbstractFacadeRemote;

public abstract class AbstractFacade<T> implements AbstractFacadeLocal<T>, AbstractFacadeRemote<T> {
	private Class<T> entityClass;

	public AbstractFacade(Class<T> entityClass) {
		this.entityClass = entityClass;
	}

	protected abstract EntityManager getEntityManager();
	
	public void create(T entity) throws ConstraintViolationException {
		getEntityManager().persist(entity);
	}
	
	public void edit(T entity) {
		getEntityManager().merge(entity);
	}
	
	public void remove(T entity) {
		getEntityManager().remove(find(((IEntity)entity).getId()));
	}
	
	public void remove(int id) {
		getEntityManager().remove(find(id));
	}
	
	public T find(Object id) {
		return getEntityManager().find(entityClass, id);
	}
	
	public List<T> findAll() {
		CriteriaQuery<T> cq = getEntityManager().getCriteriaBuilder().createQuery(entityClass);
		cq.select(cq.from(entityClass));
		return getEntityManager().createQuery(cq).getResultList();
	}
		
	@SuppressWarnings("unchecked")
	public int count() {
		CriteriaQuery<T> cq = getEntityManager().getCriteriaBuilder().createQuery(entityClass);
		Root<T> rt = cq.from(entityClass);
		cq.select((Selection<? extends T>) getEntityManager().getCriteriaBuilder().count(rt));
		Query q = getEntityManager().createQuery(cq);
		return ((Long) q.getSingleResult()).intValue();
	}
	
}
