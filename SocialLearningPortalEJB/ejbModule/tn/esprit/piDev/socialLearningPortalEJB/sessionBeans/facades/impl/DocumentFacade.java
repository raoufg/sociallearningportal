package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.Account;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Course;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Document;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Group;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.DocumentFacadeLocal;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.remote.DocumentFacadeRemote;

/**
 * Session Bean implementation class DocumentFacade
 */
@Stateless
@WebService(serviceName="DocumentFacadeWS", endpointInterface="tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.remote.DocumentFacadeRemote")
public class DocumentFacade extends AbstractFacade<Document> implements DocumentFacadeRemote, DocumentFacadeLocal {

	@PersistenceContext(unitName="SocialLearningPortal")
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public DocumentFacade() {
        super(Document.class);
    }

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Document> findByDescription(String description) {
		try {
			return getEntityManager()
					.createNamedQuery("Document.findByDescription")
					.setParameter("description", "%"+description+"%")
					.getResultList();
		} catch (Exception e) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Document> findByCourse(Course course) {
		try {
			return getEntityManager()
					.createNamedQuery("Document.findByCourse")
					.setParameter("course", course)
					.getResultList();
		} catch (Exception e) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Document> findByGroup(Group group) {
		try {
			return getEntityManager()
					.createNamedQuery("Document.findByGroup")
					.setParameter("group", group)
					.getResultList();
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	@GET
	@Path("/findByCourse/{course}/{account}")
	@Produces("application/xml")
	@WebMethod(operationName = "findByCourse")
	public List<Document> findByCourse(@PathParam("course") Course course,
			@PathParam("account") Account account) {
		// TODO Auto-generated method stub
		return null;
	}
}
