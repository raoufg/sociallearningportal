package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.remote;

import javax.ejb.Remote;
import javax.jws.WebService;
import javax.ws.rs.Path;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.Student;


@Remote
@Path("/WS-REST/StudentFacade")
@WebService(name="StudentFacadeWS")
public interface StudentFacadeRemote extends AbstractFacadeRemote<Student> {

}
