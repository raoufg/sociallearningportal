package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.remote;

import javax.ejb.Remote;
import javax.jws.WebService;
import javax.ws.rs.Path;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.Teacher;


@Remote
@Path("/WS-REST/TeacherFacade")
@WebService(name="TeacherFacadeWS")
public interface TeacherFacadeRemote extends AbstractFacadeRemote<Teacher> {

}
