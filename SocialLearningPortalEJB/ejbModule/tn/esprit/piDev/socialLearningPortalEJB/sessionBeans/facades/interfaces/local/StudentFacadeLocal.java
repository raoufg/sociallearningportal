package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.Class;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Group;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Student;


@Local
public interface StudentFacadeLocal extends AbstractFacadeLocal<Student> {

	public List<Student> findByClass(Class classe);
	public List<Student> findByGroup(Group group);
}
