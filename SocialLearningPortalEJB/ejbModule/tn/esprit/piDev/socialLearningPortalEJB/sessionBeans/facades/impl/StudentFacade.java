package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Class;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Group;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Student;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.StudentFacadeLocal;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.remote.StudentFacadeRemote;

/**
 * Session Bean implementation class CourseFacade
 */
@Stateless
@WebService(serviceName="StudentFacadeWS", endpointInterface="tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.remote.StudentFacadeRemote")
public class StudentFacade extends AbstractFacade<Student> implements StudentFacadeLocal, StudentFacadeRemote {

	@PersistenceContext(unitName="SocialLearningPortal")
	private EntityManager em;
	
	
    /**
     * Default constructor. 
     */
    
       
    public StudentFacade() {
        super(Student.class);   
    }

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Student> findByClass(Class classe) {
		try {
			return getEntityManager()
					.createNamedQuery("Student.findByClass")
					.setParameter("classe", classe)
					.getResultList();
		} catch (Exception e) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Student> findByGroup(Group group) {
		try {
			return getEntityManager()
					.createNamedQuery("Student.findByGroup")
					.setParameter("group", group)
					.getResultList();
		} catch (Exception e) {
			return null;
		}
	}

}
