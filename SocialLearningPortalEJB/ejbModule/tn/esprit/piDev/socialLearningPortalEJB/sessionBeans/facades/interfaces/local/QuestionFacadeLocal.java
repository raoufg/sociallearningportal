package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.Question;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Quiz;

@Local
public interface QuestionFacadeLocal extends AbstractFacadeLocal<Question> {
	public Quiz findQuestionsForQuiz(Quiz quiz);
	public List<Question> findByQuiz(Quiz quiz);
	public List<Question> findByTheme(String theme);
}
