package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.remote;

import javax.ejb.Remote;
import javax.jws.WebService;
import javax.ws.rs.Path;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.File;


@Remote
@Path("/WS-REST/FileFacade")
@WebService(name="AccountFacadeWS")
public interface FileFacadeRemote extends AbstractFacadeRemote<File> {
}
