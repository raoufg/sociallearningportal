package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.Admin;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.AdminFacadeLocal;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.remote.AdminFacadeRemote;

/**
 * Session Bean implementation class CourseFacade
 */
@Stateless
@WebService(serviceName="AdminFacadeWS", endpointInterface="tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.remote.AdminFacadeRemote")
public class AdminFacade extends AbstractFacade<Admin> implements AdminFacadeLocal, AdminFacadeRemote {

	@PersistenceContext(unitName="SocialLearningPortal")
	private EntityManager em;
	
	
    /**
     * Default constructor. 
     */
    
       
    public AdminFacade() {
        super(Admin.class);   
    }

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Admin> findByAdminPrivileges(String adminPrivileges) {
		try {
			return getEntityManager()
					.createNamedQuery("Admin.findByAdminPrivileges")
					.setParameter("adminPrivileges", "%"+adminPrivileges+"%")
					.getResultList();
		} catch (Exception e) {
			return null;
		}
	}

}
