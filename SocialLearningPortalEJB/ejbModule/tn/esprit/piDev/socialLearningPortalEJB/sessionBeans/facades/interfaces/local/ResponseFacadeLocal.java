package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.Question;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Response;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.StudentAssmt;


@Local
public interface ResponseFacadeLocal extends AbstractFacadeLocal<Response> {
	public Question findResponsesForQuestion(Question question);
	public List<Response> findByQuestion(Question question);
	public List<Response> findByStudentAssmt(StudentAssmt studentAssmt);
}
