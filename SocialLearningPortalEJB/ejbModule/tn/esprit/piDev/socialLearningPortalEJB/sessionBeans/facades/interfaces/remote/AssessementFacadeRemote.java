package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.remote;

import javax.ejb.Remote;
import javax.jws.WebService;
import javax.ws.rs.Path;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.Assessement;

@Remote
@Path("/WS-REST/AssessementFacade")
@WebService(name="AssessementFacadeWS")
public interface AssessementFacadeRemote extends AbstractFacadeRemote<Assessement> {
	
}
