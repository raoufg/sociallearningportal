package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Account;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Message;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.MessageFacadeLocal;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.remote.MessageFacadeRemote;

/**
 * Session Bean implementation class CourseFacade
 */
@Stateless
@WebService(serviceName="MessageFacadeWS", endpointInterface="tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.remote.MessageFacadeRemote")
public class MessageFacade extends AbstractFacade<Message> implements MessageFacadeRemote, MessageFacadeLocal {

	@PersistenceContext(unitName="SocialLearningPortal")
	private EntityManager em;
	
	
    /**
     * Default constructor. 
     */
    
       
    public MessageFacade() {
        super(Message.class);
        
    }

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Message> findByRead(boolean read) {
		try {
			return getEntityManager()
					.createNamedQuery("Message.findByRead")
					.setParameter("read", read)
					.getResultList();
		} catch (Exception e) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Message> findBySenderAccount(Account senderAccount) {
		try {
			return getEntityManager()
					.createNamedQuery("Message.findBySenderAccount")
					.setParameter("senderAccount", senderAccount)
					.getResultList();
		} catch (Exception e) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Message> findByReceiverAccount(Account receiverAccount) {
		try {
			return getEntityManager()
					.createNamedQuery("Message.findByReceiverAccount")
					.setParameter("receiverAccount", receiverAccount)
					.getResultList();
		} catch (Exception e) {
			return null;
		}
	}	

}
