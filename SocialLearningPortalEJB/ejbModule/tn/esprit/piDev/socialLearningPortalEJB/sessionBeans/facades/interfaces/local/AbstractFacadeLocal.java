package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local;

import java.util.List;

public interface AbstractFacadeLocal<T> {
	
	public void create(T entity);
	public void edit(T entity);
	public void remove(T entity);
	public void remove(int id);
	public T find(Object id);
	public List<T> findAll();
	public int count();
}
