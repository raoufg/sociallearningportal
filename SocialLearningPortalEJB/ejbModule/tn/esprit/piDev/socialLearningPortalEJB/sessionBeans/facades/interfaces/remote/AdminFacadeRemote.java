package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.remote;

import javax.ejb.Remote;
import javax.jws.WebService;
import javax.ws.rs.Path;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.Admin;


@Remote
@Path("/WS-REST/AdminFacade")
@WebService(name="AdminFacadeWS")
public interface AdminFacadeRemote extends AbstractFacadeRemote<Admin> {
}
