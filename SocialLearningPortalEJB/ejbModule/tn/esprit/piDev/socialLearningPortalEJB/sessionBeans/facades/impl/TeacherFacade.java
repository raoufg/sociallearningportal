package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.CourseSession;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Teacher;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.TeacherFacadeLocal;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.remote.TeacherFacadeRemote;

/**
 * Session Bean implementation class CourseFacade
 */
@Stateless
@WebService(serviceName="TeacherFacadeWS", endpointInterface="tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.remote.TeacherFacadeRemote")
public class TeacherFacade extends AbstractFacade<Teacher> implements TeacherFacadeLocal, TeacherFacadeRemote {

	@PersistenceContext(unitName="SocialLearningPortal")
	private EntityManager em;
	
	
    /**
     * Default constructor. 
     */
    
       
    public TeacherFacade() {
        super(Teacher.class);   
    }

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Teacher> findByCourseSession(CourseSession courseSession) {
		try {
			return getEntityManager()
					.createNamedQuery("Teacher.findByCourseSession")
					.setParameter("courseSession", courseSession)
					.getResultList();
		} catch (Exception e) {
			return null;
		}
	}

}
