package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.Account;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Message;


@Local
public interface MessageFacadeLocal extends AbstractFacadeLocal<Message> {
	
	public List<Message> findByRead(boolean read);
	public List<Message> findBySenderAccount(Account senderAccount);
	public List<Message> findByReceiverAccount(Account receiverAccount);
}
