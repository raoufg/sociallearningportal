package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.Group;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Post;


@Local
public interface PostFacadeLocal extends AbstractFacadeLocal<Post> {
	
	public List<Post> findByReplyTo(Post replyTo);
	public List<Post> findByGroup(Group group);
}
