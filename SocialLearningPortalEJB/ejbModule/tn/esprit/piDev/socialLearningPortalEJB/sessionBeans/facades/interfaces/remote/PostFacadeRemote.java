package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.remote;

import javax.ejb.Remote;
import javax.jws.WebService;
import javax.ws.rs.Path;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.Post;


@Remote
@Path("/WS-REST/PostFacade")
@WebService(name="PostFacadeWS")
public interface PostFacadeRemote extends AbstractFacadeRemote<Post> {
	
}
