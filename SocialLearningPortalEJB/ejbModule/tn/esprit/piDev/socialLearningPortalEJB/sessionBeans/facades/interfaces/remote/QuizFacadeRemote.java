package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.remote;

import javax.ejb.Remote;
import javax.jws.WebService;
import javax.ws.rs.Path;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.Quiz;


@Remote
@Path("/WS-REST/QuizFacade")
@WebService(name="QuizFacadeWS")
public interface QuizFacadeRemote extends AbstractFacadeRemote<Quiz> {
	
}
