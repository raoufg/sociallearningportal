package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.remote;

import javax.ejb.Remote;
import javax.jws.WebService;
import javax.ws.rs.Path;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.Message;


@Remote
@Path("/WS-REST/MessageFacade")
@WebService(name="MessageFacadeWS")
public interface MessageFacadeRemote extends AbstractFacadeRemote<Message> {
	
}
