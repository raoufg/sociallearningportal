package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Group;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Post;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.PostFacadeLocal;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.remote.PostFacadeRemote;

/**
 * Session Bean implementation class CourseFacade
 */
@Stateless
@WebService(serviceName="PostFacadeWS", endpointInterface="tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.remote.PostFacadeRemote")
public class PostFacade extends AbstractFacade<Post> implements PostFacadeRemote, PostFacadeLocal {

	@PersistenceContext(unitName="SocialLearningPortal")
	private EntityManager em;
	
	
    /**
     * Default constructor. 
     */
    
       
    public PostFacade() {
        super(Post.class);
        
    }

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Post> findByReplyTo(Post replyTo) {
		try {
			return getEntityManager()
					.createNamedQuery("Post.findByReplyTo")
					.setParameter("replyTo", replyTo)
					.getResultList();
		} catch (Exception e) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Post> findByGroup(Group group) {
		try {
			return getEntityManager()
					.createNamedQuery("Post.findByGroup")
					.setParameter("group", group)
					.getResultList();
		} catch (Exception e) {
			return null;
		}
	}

}
