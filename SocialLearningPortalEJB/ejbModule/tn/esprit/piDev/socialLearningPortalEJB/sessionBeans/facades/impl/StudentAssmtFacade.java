package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Assessement;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Student;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.StudentAssmt;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.StudentAssmtFacadeLocal;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.remote.StudentAssmtFacadeRemote;

/**
 * Session Bean implementation class CourseFacade
 */
@Stateless
@WebService(serviceName="StudentAssmtFacadeWS", endpointInterface="tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.remote.StudentAssmtFacadeRemote")
public class StudentAssmtFacade extends AbstractFacade<StudentAssmt> implements StudentAssmtFacadeLocal, StudentAssmtFacadeRemote {

	@PersistenceContext(unitName="SocialLearningPortal")
	private EntityManager em;
	
	
    /**
     * Default constructor. 
     */
    
       
    public StudentAssmtFacade() {
        super(StudentAssmt.class);   
    }

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<StudentAssmt> findByStudent(Student student) {
		try {
			return getEntityManager()
					.createNamedQuery("StudentAssmt.findByStudent")
					.setParameter("student", student)
					.getResultList();
		} catch (Exception e) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<StudentAssmt> findByAssessement(Assessement assessement) {
		try {
			return getEntityManager()
					.createNamedQuery("StudentAssmt.findByAssessement")
					.setParameter("assessement", assessement)
					.getResultList();
		} catch (Exception e) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<StudentAssmt> findByAssessementPassed(Boolean assessementPassed) {
		try {
			return getEntityManager()
					.createNamedQuery("StudentAssmt.findByAssessementPassed")
					.setParameter("assessementPassed", assessementPassed)
					.getResultList();
		} catch (Exception e) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<StudentAssmt> findByPassingAssessementNow(
			Boolean passingAssessementNow) {
		try {
			return getEntityManager()
					.createNamedQuery("StudentAssmt.findByPassingAssessementNow")
					.setParameter("passingAssessementNow", passingAssessementNow)
					.getResultList();
		} catch (Exception e) {
			return null;
		}
	}

}
