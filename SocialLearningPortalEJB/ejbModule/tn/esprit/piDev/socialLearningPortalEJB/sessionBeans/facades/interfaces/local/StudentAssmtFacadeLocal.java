package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.Assessement;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Student;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.StudentAssmt;


@Local
public interface StudentAssmtFacadeLocal extends AbstractFacadeLocal<StudentAssmt> {
	
	public List<StudentAssmt> findByStudent(Student student);
	public List<StudentAssmt> findByAssessement(Assessement assessement);
	public List<StudentAssmt> findByAssessementPassed(Boolean assessementPassed);
	public List<StudentAssmt> findByPassingAssessementNow(Boolean passingAssessementNow);
}
