package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.PathParam;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Account;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Course;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Document;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Teacher;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.CourseFacadeLocal;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.remote.CourseFacadeRemote;

/**
 * Session Bean implementation class CourseFacade
 */
@Stateless
@WebService(serviceName="CourseFacadeWS", endpointInterface="tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.remote.CourseFacadeRemote")
public class CourseFacade extends AbstractFacade<Course> implements CourseFacadeRemote, CourseFacadeLocal {

	@PersistenceContext(unitName="SocialLearningPortal")
	private EntityManager em;
	
	
    /**
     * Default constructor. 
     */
    
       
    public CourseFacade() {
        super(Course.class);
        
    }

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Course> findByName(String name) {
		try {
			return getEntityManager()
					.createNamedQuery("Course.findByName")
					.setParameter("name", "%"+name+"%")
					.getResultList();
		} catch (Exception e) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Course> findBySubject(String subject) {
		try {
			return getEntityManager()
					.createNamedQuery("Course.findBySubject")
					.setParameter("subject", "%"+subject+"%")
					.getResultList();
		} catch (Exception e) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Course> findByCreator(Teacher creator) {
		try {
			return getEntityManager()
					.createNamedQuery("Course.findByCreator")
					.setParameter("creator", creator)
					.getResultList();
		} catch (Exception e) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Course> findByDocument(Document document) {
		try {
			return getEntityManager()
					.createNamedQuery("Course.findByDocument")
					.setParameter("document", document)
					.getResultList();
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<Course> findByName(@PathParam("name") String name,
			@PathParam("account") Account account) {
		// TODO Auto-generated method stub
		return null;
	}

}
