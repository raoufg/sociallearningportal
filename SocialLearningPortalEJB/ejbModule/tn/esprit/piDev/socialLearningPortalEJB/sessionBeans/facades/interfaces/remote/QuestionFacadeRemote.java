package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.remote;

import javax.ejb.Remote;
import javax.jws.WebService;
import javax.ws.rs.Path;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.Question;


@Remote
@Path("/WS-REST/QuestionFacade")
@WebService(name="QuestionFacadeWS")
public interface QuestionFacadeRemote extends AbstractFacadeRemote<Question> {
	
}
