package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.impl;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Assessement;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.CourseSession;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Quiz;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Teacher;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.AssessementFacadeLocal;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.remote.AssessementFacadeRemote;

/**
 * Session Bean implementation class GroupeFacade
 */
@Stateless
@WebService(serviceName="AssessementFacadeWS", endpointInterface="tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.remote.AssessementFacadeRemote")
public class AssessementFacade extends AbstractFacade<Assessement> implements AssessementFacadeRemote, AssessementFacadeLocal {
	
	@PersistenceContext(unitName="SocialLearningPortal")
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public AssessementFacade() {
        super(Assessement.class);
    }
    
	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Assessement> findByName(String name) {
		try {
			return getEntityManager()
					.createNamedQuery("Assessement.findByName")
					.setParameter("name", "%"+name+"%")
					.getResultList();
		} catch (Exception e) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Assessement> findByBeginDate(Date beginDate) {
		try {
			return getEntityManager()
					.createNamedQuery("Assessement.findByBeginDate")
					.setParameter("beginDate", beginDate)
					.getResultList();
		} catch (Exception e) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Assessement> findByQuiz(Quiz quiz) {
		try {
			return getEntityManager()
					.createNamedQuery("Assessement.findByQuiz")
					.setParameter("quiz", quiz)
					.getResultList();
		} catch (Exception e) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Assessement> findByCourseSession(CourseSession courseSession) {
		try {
			return getEntityManager()
					.createNamedQuery("Assessement.findByCourseSession")
					.setParameter("courseSession", courseSession)
					.getResultList();
		} catch (Exception e) {
			return null;
		}
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<Assessement> findByTeacher(Teacher teacher) {
		try {
			return getEntityManager()
					.createNamedQuery("Assessement.findByTeacher")
					.setParameter("teacher", teacher)
					.getResultList();
		} catch (Exception e) {
			return null;
		}
	}
}
