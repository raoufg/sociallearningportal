package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.remote;

import javax.ejb.Remote;
import javax.jws.WebService;
import javax.ws.rs.Path;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.Response;


@Remote
@Path("/WS-REST/ResponseFacade")
@WebService(name="ResponseFacadeWS")
public interface ResponseFacadeRemote extends AbstractFacadeRemote<Response> {
	
}
