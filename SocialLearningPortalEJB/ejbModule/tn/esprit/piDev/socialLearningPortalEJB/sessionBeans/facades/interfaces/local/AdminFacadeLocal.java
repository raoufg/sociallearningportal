package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.Admin;


@Local
public interface AdminFacadeLocal extends AbstractFacadeLocal<Admin> {
	public List<Admin> findByAdminPrivileges(String adminPrivileges);
}
