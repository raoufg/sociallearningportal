package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local;

import javax.ejb.Local;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.Class;


@Local
public interface ClassFacadeLocal extends AbstractFacadeLocal<Class> {
}
