package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.remote;

/*
import java.util.List;

import javax.jws.WebMethod;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.Account;
*/

public interface AbstractFacadeRemote<T> {
	
	/*
	@POST
	@Path("/create")
	@Consumes({"application/xml","application/xml"})
	@WebMethod(operationName="create")
	public void create(T entity, Account account);
	
	@POST
	@Path("/edit")
	@Consumes({"application/xml","application/xml"})
	@WebMethod(operationName="edit")
	public void edit(T entity,Account account);
	
	@POST
	@Path("/remove")
	@Consumes({"application/xml","application/xml"})
	@WebMethod(operationName="remove")
	public void remove(T entity, Account account);
	
	
	@POST
	@Path("/find")
	@Produces("application/xml")
	@Consumes({"application/xml","application/xml"})
	@WebMethod(operationName="find")
	public T find(Object id, Account account);
	
	@POST
	@Path("/findAll")
	@Produces("application/xml")
	@Consumes("application/xml")
	@WebMethod(operationName="findAll")
	public List<T> findAll(Account account);
	
	@POST
	@Path("/count")
	@Produces("text/plain")
	@Consumes("application/xml")
	@WebMethod(operationName="count")
	public int count(Account account);
	*/
}
