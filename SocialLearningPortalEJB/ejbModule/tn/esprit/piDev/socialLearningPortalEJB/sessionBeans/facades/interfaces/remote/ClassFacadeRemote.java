package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.remote;

import javax.ejb.Remote;
import javax.jws.WebService;
import javax.ws.rs.Path;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.Class;


@Remote
@Path("/WS-REST/ClassFacade")
@WebService(name="ClassFacadeWS")
public interface ClassFacadeRemote extends AbstractFacadeRemote<Class> {
}
