package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Question;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Response;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.StudentAssmt;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.ResponseFacadeLocal;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.remote.ResponseFacadeRemote;

/**
 * Session Bean implementation class ResponseFacade
 */
@Stateless
@WebService(serviceName="ResponseFacadeWS", endpointInterface="tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.remote.ResponseFacadeRemote")
public class ResponseFacade extends AbstractFacade<Response> implements
		ResponseFacadeLocal, ResponseFacadeRemote {

	@PersistenceContext(unitName = "SocialLearningPortal")
	private EntityManager em;

	/**
	 * Default constructor.
	 */
	public ResponseFacade() {
		super(Response.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	@SuppressWarnings("unchecked")
	public Question findResponsesForQuestion(Question question) {
		question.setResponses(getEntityManager()
				.createNamedQuery("Response.findByQuestion")
				.setParameter("question", question).getResultList());
		return question;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Response> findByQuestion(Question question) {
		try {
			return getEntityManager()
			.createNamedQuery("Response.findByQuestion")
			.setParameter("question", question)
			.getResultList();
		} catch (Exception e) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Response> findByStudentAssmt(StudentAssmt studentAssmt) {
		try {
			return getEntityManager()
			.createNamedQuery("Response.findByStudentAssmt")
			.setParameter("studentAssmt", studentAssmt)
			.getResultList();
		} catch (Exception e) {
			return null;
		}
	}
}
