package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.Course;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Document;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Teacher;


@Local
public interface CourseFacadeLocal extends AbstractFacadeLocal<Course> {
	
	public List<Course> findByName(String name);
	public List<Course> findBySubject(String subject);
	public List<Course> findByCreator(Teacher creator);
	public List<Course> findByDocument(Document document);
}
