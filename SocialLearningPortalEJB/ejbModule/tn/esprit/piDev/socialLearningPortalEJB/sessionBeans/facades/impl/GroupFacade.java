package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Account;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Group;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Student;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.GroupFacadeLocal;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.remote.GroupFacadeRemote;

/**
 * Session Bean implementation class GroupeFacade
 */
@Stateless
@WebService(serviceName="GroupFacadeWS", endpointInterface="tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.remote.GroupFacadeRemote")
public class GroupFacade extends AbstractFacade<Group> implements GroupFacadeRemote, GroupFacadeLocal {
	
	@PersistenceContext(unitName="SocialLearningPortal")
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public GroupFacade() {
        super(Group.class);
    }
    
	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Group> findByName(String name) {
		try {
			return getEntityManager()
					.createNamedQuery("Group.findByName")
					.setParameter("name", "%"+name+"%")
					.getResultList();
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<Group> findByNameOrType(String name, String type) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Group> findByNameOrTypeAndOpen(String name, String type,
			boolean open) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Group> findByStudent(Student student) {
		try {
			return getEntityManager()
					.createNamedQuery("Group.findByStudent")
					.setParameter("student", student)
					.getResultList();
		} catch (Exception e) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Group> findByCreator(Account account) {
		try {
			return getEntityManager()
					.createNamedQuery("Group.findByCreator")
					.setParameter("creator", account)
					.getResultList();
		} catch (Exception e) {
			return null;
		}
	}
}
