package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.Account;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Group;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Student;


@Local
public interface GroupFacadeLocal extends AbstractFacadeLocal<Group> {
	public List<Group> findByName(String name);
	public List<Group> findByNameOrType(String name, String type);
	public List<Group> findByNameOrTypeAndOpen(String name, String type, boolean open);
	public List<Group> findByStudent(Student student);
	public List<Group> findByCreator(Account account);
}
