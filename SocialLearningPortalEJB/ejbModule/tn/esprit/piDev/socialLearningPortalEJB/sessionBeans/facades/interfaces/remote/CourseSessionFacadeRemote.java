package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.remote;

import javax.ejb.Remote;
import javax.jws.WebService;
import javax.ws.rs.Path;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.CourseSession;


@Remote
@Path("/WS-REST/CourseSessionFacade")
@WebService(name="CourseSessionFacadeWS")
public interface CourseSessionFacadeRemote extends AbstractFacadeRemote<CourseSession> {

}
