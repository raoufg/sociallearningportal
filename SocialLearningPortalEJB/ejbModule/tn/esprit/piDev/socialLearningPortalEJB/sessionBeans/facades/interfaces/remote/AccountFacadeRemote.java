package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.remote;

import javax.ejb.Remote;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.Account;


@Remote
@Path("/WS-REST/AccountFacade")
@WebService(name="AccountFacadeWS")
public interface AccountFacadeRemote extends AbstractFacadeRemote<Account> {
	
	
	@POST
	@Path("/findByUsernameAndPasswd")
	@Produces("application/xml")
	@Consumes({"text/plain","text/plain"})
	@WebMethod(operationName="findByUsernameAndPasswd")
	public Account findByUsernameAndPasswd(String username, String passwd);
	
	@GET
	@Path("/test/{firstname}/{lastname}")
	@Produces("text/plain")
	@WebMethod(operationName="testAccount")
	public String testAccount(@PathParam("firstname") String firstname, @PathParam("lastname") String lastname);
}
