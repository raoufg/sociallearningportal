package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.impl;

import javax.ejb.Stateless;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Class;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.ClassFacadeLocal;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.remote.ClassFacadeRemote;

/**
 * Session Bean implementation class CourseFacade
 */
@Stateless
@WebService(serviceName="ClassFacadeWS", endpointInterface="tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.remote.ClassFacadeRemote")
public class ClassFacade extends AbstractFacade<Class> implements ClassFacadeLocal, ClassFacadeRemote {

	@PersistenceContext(unitName="SocialLearningPortal")
	private EntityManager em;
	
	
    /**
     * Default constructor. 
     */
    
       
    public ClassFacade() {
        super(Class.class);   
    }

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}
}
