package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.remote;

import java.util.List;

import javax.ejb.Remote;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.Account;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Course;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Document;


@Remote
@Path("/WS-REST/DocumentFacade")
@WebService(name="DocumentFacadeWS")
public interface DocumentFacadeRemote extends AbstractFacadeRemote<Document> {
	
	@GET
	@Path("/findByCourse/{course}/{account}")
	@Produces("application/xml")
	@WebMethod(operationName="findByCourse")
	public List<Document> findByCourse(@PathParam("course") Course course, @PathParam("account") Account account);

}
