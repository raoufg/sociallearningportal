package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Question;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Quiz;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Teacher;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.QuizFacadeLocal;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.remote.QuizFacadeRemote;

/**
 * Session Bean implementation class QuizFacade
 */
@Stateless
@WebService(serviceName="QuizFacadeWS", endpointInterface="tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.remote.QuizFacadeRemote")
public class QuizFacade extends AbstractFacade<Quiz> implements QuizFacadeLocal, QuizFacadeRemote {

	
	@PersistenceContext(unitName="SocialLearningPortal")
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public QuizFacade() {
        super(Quiz.class);
    }
       

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}
	
	@SuppressWarnings("unchecked")
	public Question findQuizsForQuestion(Question question){
		question.setQuizs(getEntityManager()
				.createNamedQuery("Quiz.findByQuestion")
				.setParameter("question", question)
				.getResultList());
		return question;
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<Quiz> findByName(String name) {
		try {
			return getEntityManager()
					.createNamedQuery("Quiz.findByName")
					.setParameter("name", "%"+name+"%")
					.getResultList();
		} catch (Exception e) {
			return null;
		}
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<Quiz> findByQuestion(Question question) {
		try{
			return getEntityManager()
			.createNamedQuery("Quiz.findByQuestion")
			.setParameter("question", question)
			.getResultList();
		} catch (Exception e) {
			return null;
		}
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<Quiz> findByCreator(Teacher teacher) {
		try{
			return getEntityManager()
			.createNamedQuery("Quiz.findByCreator")
			.setParameter("creator", teacher)
			.getResultList();
		} catch (Exception e) {
			return null;
		}
			
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<Quiz> findByCreatorOrPublicQuiz(Teacher teacher, boolean publicQuiz) {
		try{
			return getEntityManager()
			.createNamedQuery("Quiz.findByCreatorOrPublicQuiz")
			.setParameter("creator", teacher)
			.setParameter("publicQuiz", publicQuiz)
			.getResultList();
		} catch (Exception e) {
			return null;
		}
	}
}
