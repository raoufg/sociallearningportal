package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.impl;

import javax.ejb.Stateless;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.File;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.FileFacadeLocal;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.remote.FileFacadeRemote;

/**
 * Session Bean implementation class CourseFacade
 */
@Stateless
@WebService(serviceName="FileFacadeWS", endpointInterface="tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.remote.FileFacadeRemote")
public class FileFacade extends AbstractFacade<File> implements FileFacadeLocal, FileFacadeRemote {

	@PersistenceContext(unitName="SocialLearningPortal")
	private EntityManager em;
	
	
    /**
     * Default constructor. 
     */
    
       
    public FileFacade() {
        super(File.class);   
    }

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}
}
