package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.remote;

import javax.ejb.Remote;
import javax.jws.WebService;
import javax.ws.rs.Path;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.Group;


@Remote
@Path("/WS-REST/GroupFacade")
@WebService(name="GroupFacadeWS")
public interface GroupFacadeRemote extends  AbstractFacadeRemote<Group> {

}
