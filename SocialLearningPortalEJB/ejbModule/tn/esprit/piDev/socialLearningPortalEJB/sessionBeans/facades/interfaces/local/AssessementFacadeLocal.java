package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.Assessement;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.CourseSession;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Quiz;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Teacher;

@Local
public interface AssessementFacadeLocal extends AbstractFacadeLocal<Assessement> {

	public List<Assessement> findByName(String name);
	public List<Assessement> findByBeginDate(Date beginDate);
	public List<Assessement> findByQuiz(Quiz quiz);
	public List<Assessement> findByCourseSession(CourseSession courseSession);
	public List<Assessement> findByTeacher(Teacher teacher);
}
