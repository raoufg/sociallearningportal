package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Account;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Message;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.AccountFacadeLocal;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.remote.AccountFacadeRemote;

/**
 * Session Bean implementation class CourseFacade
 */
@Stateless
@WebService(serviceName="AccountFacadeWS", endpointInterface="tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.remote.AccountFacadeRemote")
public class AccountFacade extends AbstractFacade<Account> implements
		AccountFacadeLocal, AccountFacadeRemote {

	@PersistenceContext(unitName = "SocialLearningPortal")
	private EntityManager em;

	/**
	 * Default constructor.
	 */

	public AccountFacade() {
		super(Account.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Account> findByUsername(String username) {
		try {
			return  getEntityManager()
					.createNamedQuery("Account.findByUsername")
					.setParameter("username", "%"+username+"%")
					.getResultList();
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public Account findByUsernameAndPasswd(String username, String passwd) {
		try {
			return (Account) getEntityManager()
					.createNamedQuery("Account.findByUsernameAndPasswd")
					.setParameter("username", username)
					.setParameter("passwd", passwd)
					.getSingleResult();
		} catch (Exception e) {
			System.err.println(e.getMessage());
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Account> findByLastNameOrFirstName(String lastName,
			String firstName) {
		try {
			return getEntityManager()
					.createNamedQuery("Account.findByLastNameOrFirstName")
					.setParameter("lastName", "%"+lastName+"%")
					.setParameter("firstName", "%"+firstName+"%")
					.getResultList();
		} catch (Exception e) {
			return null;
		}
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<Account> findByMessageReceived(Message message) {
		try {
			return getEntityManager()
					.createNamedQuery("Account.findByMessageReceived")
					.setParameter("message", message)
					.getResultList();
		} catch (Exception e) {
			return null;
		}
	}


	@Override
	public String testAccount(String firstname, String lastname) {
		return "Hello "+firstname+" "+lastname+" from AccountFacade WS";
	}
}
