package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.Account;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Message;


@Local
public interface AccountFacadeLocal extends AbstractFacadeLocal<Account> {
	public Account findByUsernameAndPasswd(String username, String passwd);
	public List<Account> findByUsername(String username);
	public List<Account> findByLastNameOrFirstName(String lastName, String firstName);
	public List<Account> findByMessageReceived(Message message);
}
