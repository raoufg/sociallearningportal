package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local;

import javax.ejb.Local;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.File;


@Local
public interface FileFacadeLocal extends AbstractFacadeLocal<File> {
}
