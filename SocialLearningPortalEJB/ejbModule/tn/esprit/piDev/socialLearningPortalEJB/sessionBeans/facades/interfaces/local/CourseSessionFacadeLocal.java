package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.Course;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.CourseSession;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Student;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Teacher;


@Local
public interface CourseSessionFacadeLocal extends AbstractFacadeLocal<CourseSession> {
	
	public List<CourseSession> findByCourse(Course course);
	public List<CourseSession> findByTeacher(Teacher teacher);
	public List<CourseSession> findByStudent(Student student);
	public List<CourseSession> findByCreator(Teacher creator);
}
