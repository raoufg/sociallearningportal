package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.Course;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Document;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Group;


@Local
public interface DocumentFacadeLocal extends AbstractFacadeLocal<Document> {
	
	public List<Document> findByDescription(String description);
	public List<Document> findByCourse(Course course);
	public List<Document> findByGroup(Group group);
}
