package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.services.interfaces.remote;

import javax.ejb.Remote;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.Account;

@Remote
@Path("/WS-REST/UsersManagementService")
@WebService(name="UsersManagementWS")
public interface UsersManagementRemote {
	
	@POST
	@Path("/authenticate")
	@Produces("application/xml")
	@Consumes({"text/plain","text/plain"})
	@WebMethod(operationName="authenticate")
	public Account authenticate(String username, String passwd);
	
	@GET
	@Path("/testUsersManagement/{firstname}/{lastname}")
	@Produces("text/plain")
	@WebMethod(operationName="testUsersManagement")
	public String testUsersManagement(@PathParam("firstname") String firstname, @PathParam("lastname") String lastname);
}
