package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.services.interfaces.remote;

import javax.ejb.Remote;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

@Remote
@Path("/WS-REST/MobileServices")
@WebService(name="MobileWS")
public interface MobileServicesRemote extends UsersManagementRemote , AssessementManagementRemote {

	@GET
	@Path("/test/{firstname}/{lastname}")
	@Produces("text/plain")
	@WebMethod(operationName="test")
	public String test(@PathParam("firstname") String firstname, @PathParam("lastname") String lastname);
}
