package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.services.impl;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebService;
import javax.ws.rs.PathParam;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Account;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.services.interfaces.local.AssessementManagementLocal;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.services.interfaces.local.UsersManagementLocal;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.services.interfaces.remote.MobileServicesRemote;

/**
 * Session Bean implementation class MobileService
 */
@Stateless
@WebService(serviceName="MobileWS", endpointInterface="tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.services.interfaces.remote.MobileServicesRemote")
public class MobileServices implements MobileServicesRemote {

    /**
     * Default constructor. 
     */
	@EJB
	private UsersManagementLocal usersManagement;
	@EJB
	private AssessementManagementLocal assessementManagement;
	
    public MobileServices() {
    }

	@Override
    public Account authenticate(String username, String passwd) {
        return usersManagement.authenticate(username, passwd);
    }

	/**
     * TESTERS
     */
	@Override
	public String testUsersManagement(@PathParam("firstname") String firstname,
			@PathParam("lastname") String lastname) {
		return usersManagement.testUsersManagement(firstname, lastname);
	}

	@Override
	public String testAssessement(@PathParam("firstname") String firstname,
			@PathParam("lastname") String lastname) {
		return assessementManagement.testAssessement(firstname, lastname);
	}

	@Override
	public String test(@PathParam("firstname") String firstname,
			@PathParam("lastname") String lastname) {
		return "Hello "+firstname+" "+lastname+" from Mobile Service";
	}

}
