package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.services.impl;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebService;
import javax.ws.rs.PathParam;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Account;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.AccountFacadeLocal;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.services.interfaces.local.UsersManagementLocal;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.services.interfaces.remote.UsersManagementRemote;

/**
 * Session Bean implementation class AssessementManagement
 */
@Stateless
@WebService(serviceName="UsersManagementWS", endpointInterface="tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.services.interfaces.remote.UsersManagementRemote")
public class UsersManagement implements UsersManagementLocal, UsersManagementRemote {

    
	@EJB
	private AccountFacadeLocal accountFacade;
	
    public UsersManagement() {
    }

	@Override
	public Account authenticate(String username, String passwd) {
		return accountFacade.findByUsernameAndPasswd(username, passwd);
	}

	@Override
	public String testUsersManagement(@PathParam("firstname") String firstname, @PathParam("lastname") String lastname) {
		return "Hello "+firstname+" "+lastname+ " from users management WS";
	}
	
	

}
