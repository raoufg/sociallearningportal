package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.services.interfaces.remote;

import javax.ejb.Remote;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

@Remote
@Path("/WS-REST/AssessementManagement")
@WebService(name="AssessementManagementWS")
public interface AssessementManagementRemote {

	@GET
	@Path("/testAssessement/{firstname}/{lastname}")
	@Produces("text/plain")
	@WebMethod(operationName="testAssessement")
	public String testAssessement(@PathParam("firstname") String firstname, @PathParam("lastname") String lastname);
}
