package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.services.interfaces.local;

import javax.ejb.Local;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Account;

@Local
public interface UsersManagementLocal {
	
	public Account authenticate(String username, String passwd);
	public String testUsersManagement(String firstname, String lastname);
}
