package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.services.interfaces.local;

import javax.ejb.Local;

@Local
public interface AssessementManagementLocal {
	public String testAssessement(String firstname, String lastname);
}
