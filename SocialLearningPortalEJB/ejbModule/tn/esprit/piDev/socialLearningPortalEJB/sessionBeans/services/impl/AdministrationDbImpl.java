package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.services.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.services.interfaces.local.AdministrationDbLocal;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.services.interfaces.remote.AdministrationDbRemote;

/**
 * Session Bean implementation class AdministrationDbImpl
 */
@Stateless
public class AdministrationDbImpl implements AdministrationDbLocal, AdministrationDbRemote {

	@PersistenceContext(unitName = "SocialLearningPortal")
	private EntityManager em;
	private Connection connection;
	
    /**
     * Default constructor. 
     */
    public AdministrationDbImpl() {
    	
    }

	@Override
	public String afficherPrivilegesSysteme(String username) {
		if(connection == null) {
			//connection = ((SessionImpl) em.getDelegate()).connection();
		}
		try {
			CallableStatement statement = connection.prepareCall("{call USER_PRIVS(?)}");
			statement.setString(1, username);
			statement.execute();
			ResultSet resultSet = statement.getResultSet();
			String results="";
			while(resultSet.next()){
				for(int i=0; i<resultSet.getFetchSize();i++){
					if(i!=resultSet.getFetchSize()-1) results+=resultSet.getString(i)+" | ";
					else results+=resultSet.getString(i);
				}
				results+="\n";
			}
			return results;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		
	}

	@Override
	public String afficherPrivilegesRole(String username) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String afficherInformationUser(String username) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String afficherInfoSession() {
		// TODO Auto-generated method stub
		return null;
	}

}
