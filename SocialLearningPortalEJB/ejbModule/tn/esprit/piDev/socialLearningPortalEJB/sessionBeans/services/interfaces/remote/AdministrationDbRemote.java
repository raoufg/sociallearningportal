package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.services.interfaces.remote;

import javax.ejb.Remote;

@Remote
public interface AdministrationDbRemote {
	public String afficherPrivilegesSysteme(String username);
	public String afficherPrivilegesRole(String username);
	public String afficherInformationUser(String username);
	public String afficherInfoSession();
}
