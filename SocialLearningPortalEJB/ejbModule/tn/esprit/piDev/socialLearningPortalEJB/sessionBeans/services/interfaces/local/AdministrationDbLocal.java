package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.services.interfaces.local;

import javax.ejb.Local;

@Local
public interface AdministrationDbLocal {
	public String afficherPrivilegesSysteme(String username);
	public String afficherPrivilegesRole(String username);
	public String afficherInformationUser(String username);
	public String afficherInfoSession();
}
