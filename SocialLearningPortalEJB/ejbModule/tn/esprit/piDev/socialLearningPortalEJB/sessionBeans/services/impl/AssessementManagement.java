package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.services.impl;

import javax.ejb.Stateless;
import javax.ws.rs.PathParam;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.services.interfaces.local.AssessementManagementLocal;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.services.interfaces.remote.AssessementManagementRemote;

/**
 * Session Bean implementation class AssessementManagement
 */
@Stateless
public class AssessementManagement implements AssessementManagementRemote, AssessementManagementLocal {

    /**
     * Default constructor. 
     */
    public AssessementManagement() {
    }

	@Override
	public String testAssessement(@PathParam("firstname") String firstname,
			@PathParam("lastname") String lastname) {
		return "Hello "+firstname +" "+ lastname+" from AssessementManagement WS";
	}

}
