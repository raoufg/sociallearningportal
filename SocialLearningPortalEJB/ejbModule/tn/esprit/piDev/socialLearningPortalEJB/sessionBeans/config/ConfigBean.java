package tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.config;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import tn.esprit.piDev.socialLearningPortalEJB.entitys.Admin;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Class;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Student;
import tn.esprit.piDev.socialLearningPortalEJB.entitys.Teacher;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.AccountFacadeLocal;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.AdminFacadeLocal;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.ClassFacadeLocal;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.StudentFacadeLocal;
import tn.esprit.piDev.socialLearningPortalEJB.sessionBeans.facades.interfaces.local.TeacherFacadeLocal;
import tn.esprit.piDev.socialLearningPortalEJB.utils.MD5;

@Singleton
@Startup
public class ConfigBean {

	@EJB
	private AccountFacadeLocal accountFacadeLocal;
	@EJB
	private StudentFacadeLocal studentFacadeLocal;
	@EJB
	private TeacherFacadeLocal teacherFacadeLocal;
	@EJB
	private AdminFacadeLocal adminFacadeLocal;
	@EJB
	private ClassFacadeLocal classFacadeLocal;

	public ConfigBean() {
	}

	@PostConstruct
	public void initData() {

		if (classFacadeLocal.count() == 0 || accountFacadeLocal.count() == 0) {

			
			classFacadeLocal.create(new Class(1, "A", 1));
			classFacadeLocal.create(new Class(1, "A", 2));
			classFacadeLocal.create(new Class(1, "A", 3));
			classFacadeLocal.create(new Class(2, "A", 1));
			classFacadeLocal.create(new Class(2, "A", 2));
			classFacadeLocal.create(new Class(3, "B", 1));
			classFacadeLocal.create(new Class(2, "B", 1));
			classFacadeLocal.create(new Class(4, "INFOA", 1));
			classFacadeLocal.create(new Class(4, "INFOA", 2));
			classFacadeLocal.create(new Class(4, "INFOA", 3));
			classFacadeLocal.create(new Class(5, "INFOA", 1));
			classFacadeLocal.create(new Class(3, "TELECOM", 1));
			classFacadeLocal.create(new Class(3, "TELECOM", 2));
			classFacadeLocal.create(new Class(4, "TELECOM", 1));
			classFacadeLocal.create(new Class(5, "TELECOM", 1));

			String passwd = MD5.getMD5("123456"); 
			
			studentFacadeLocal.create(new Student("raoufg", passwd, "Raouf", "Gharbi", null, null, null, "abderraouf.gharbi@esprit.tn", null, true, null, classFacadeLocal.find(1)));
			studentFacadeLocal.create(new Student("hamzas", passwd, "Hamza", "Shil", null, null, null, "hamza.shil@esprit.tn", null, true, null, classFacadeLocal.find(1)));
			studentFacadeLocal.create(new Student("majedd", passwd, "Majed", "Derouiche", null, null, null, "majed.derouiche@esprit.tn", null, true, null, classFacadeLocal.find(1)));
			studentFacadeLocal.create(new Student("majdam", passwd, "Majda", "Mnafgui", null, null, null, "majda.mnafgui@esprit.tn", null, false, null, classFacadeLocal.find(1)));
			for(int i=0;i<50;i++)
				studentFacadeLocal.create(new Student("foulen.fleni."+i, passwd, "Foulen", "Fouleni."+i, null, null, null, "foulen.flen."+i+"@esprit.tn", null, true, null, classFacadeLocal.findAll().get(classFacadeLocal.findAll().size() - (classFacadeLocal.findAll().size()/(i+1)))));
			
			
			teacherFacadeLocal.create(new Teacher("mouna", passwd, "Mouna", "Makni", null, null, null, "mouna.makni@esprit.tn", null, false, null));
			
			teacherFacadeLocal.create(new Teacher("dalib", passwd, "Dali", "Bettaieb", null, null, null, "dali.bettaieb@esprit.tn", null, true, null));
			
			
			Admin admin = new Admin();
			admin.setUsername("admin");
			admin.setPasswd(MD5.getMD5("admin"));
			admin.setEmailAddress("admin@esprit.tn");
			adminFacadeLocal.create(admin);

		}
	}
}
